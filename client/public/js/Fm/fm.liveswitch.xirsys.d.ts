//
// Title: LiveSwitch for JavaScript
// Version: 1.2.0.1018
// Copyright Frozen Mountain Software 2011+
//
declare namespace fm.liveswitch.xirsys.v2 {
    /**
     <div>
     A XirSys v2 client.
     </div>

    */
    class Client {
        getTypeString(): string;
        private fmliveswitchxirsysv2ClientInit;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-constructor'>&nbsp;</span>**/
        /**
         <div>
         Initializes a new instance of the `fm.liveswitch.xirsys.v2.client` class.
         </div>

        @param {string} ident The "ident" value.
        @param {string} secret The "secret" value.
        @param {string} domain The "domain" value.
        @param {string} application The "application" value.
        @param {string} room The "room" value.
        @return {}
        */
        constructor(ident: string, secret: string, domain: string, application: string, room: string);
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-constructor'>&nbsp;</span>**/
        /**
         <div>
         Initializes a new instance of the `fm.liveswitch.xirsys.v2.client` class.
         </div>

        @param {string} ident The "ident" value.
        @param {string} secret The "secret" value.
        @param {string} domain The "domain" value.
        @param {string} application The "application" value.
        @return {}
        */
        constructor(ident: string, secret: string, domain: string, application: string);
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-constructor'>&nbsp;</span>**/
        /**
         <div>
         Initializes a new instance of the `fm.liveswitch.xirsys.v2.client` class.
         </div>

        @param {string} ident The "ident" value.
        @param {string} secret The "secret" value.
        @param {string} domain The "domain" value.
        @return {}
        */
        constructor(ident: string, secret: string, domain: string);
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-constructor'>&nbsp;</span>**/
        /**
         <div>
         Initializes a new instance of the `fm.liveswitch.xirsys.v2.client` class.
         </div>

        @param {string} ident The "ident" value.
        @param {string} secret The "secret" value.
        @param {string} domain The "domain" value.
        @param {string} application The "application" value.
        @param {string} room The "room" value.
        @param {boolean} secure The "secure" value.
        @return {}
        */
        constructor(ident: string, secret: string, domain: string, application: string, room: string, secure: boolean);
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getDefaultEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Gets the default HTTP endpoint.
         Defaults to "https://service.xirsys.com/ice".
         </div>


        @return {string}
        */
        static getDefaultEndpoint(): string;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setDefaultEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Sets the default HTTP endpoint.
         Defaults to "https://service.xirsys.com/ice".
         </div>


        @param {string} value
        @return {void}
        */
        static setDefaultEndpoint(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getApplication'>&nbsp;</span>**/
        /**
         <div>
         Gets the "application" value.
         Defaults to "default".
         </div>


        @return {string}
        */
        getApplication(): string;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getDomain'>&nbsp;</span>**/
        /**
         <div>
         Gets the "domain" value.
         </div>


        @return {string}
        */
        getDomain(): string;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Gets the HTTP endpoint.
         </div>


        @return {string}
        */
        getEndpoint(): string;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getIceServers'>&nbsp;</span>**/
        /**
         <div>
         Gets an array of XirSys ICE servers.
         </div>

        @return {fm.liveswitch.Future<fm.liveswitch.IceServer[]>}
        */
        getIceServers(): fm.liveswitch.Future<fm.liveswitch.IceServer[]>;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getIdent'>&nbsp;</span>**/
        /**
         <div>
         Gets the "ident" value.
         </div>


        @return {string}
        */
        getIdent(): string;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getRoom'>&nbsp;</span>**/
        /**
         <div>
         Gets the "room" value.
         Defaults to "default".
         </div>


        @return {string}
        */
        getRoom(): string;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getSecret'>&nbsp;</span>**/
        /**
         <div>
         Gets the "secret" value.
         </div>


        @return {string}
        */
        getSecret(): string;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-getSecure'>&nbsp;</span>**/
        /**
         <div>
         Gets the "secure" value.
         Defaults to <c>true</c>.
         </div>


        @return {boolean}
        */
        getSecure(): boolean;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setApplication'>&nbsp;</span>**/
        /**
         <div>
         Sets the "application" value.
         Defaults to "default".
         </div>


        @param {string} value
        @return {void}
        */
        setApplication(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setDomain'>&nbsp;</span>**/
        /**
         <div>
         Sets the "domain" value.
         </div>


        @param {string} value
        @return {void}
        */
        setDomain(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Sets the HTTP endpoint.
         </div>


        @param {string} value
        @return {void}
        */
        setEndpoint(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setIdent'>&nbsp;</span>**/
        /**
         <div>
         Sets the "ident" value.
         </div>


        @param {string} value
        @return {void}
        */
        setIdent(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setRoom'>&nbsp;</span>**/
        /**
         <div>
         Sets the "room" value.
         Defaults to "default".
         </div>


        @param {string} value
        @return {void}
        */
        setRoom(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setSecret'>&nbsp;</span>**/
        /**
         <div>
         Sets the "secret" value.
         </div>


        @param {string} value
        @return {void}
        */
        setSecret(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v2.Client-setSecure'>&nbsp;</span>**/
        /**
         <div>
         Sets the "secure" value.
         Defaults to <c>true</c>.
         </div>


        @param {boolean} value
        @return {void}
        */
        setSecure(value: boolean): void;
    }
}
declare namespace fm.liveswitch.xirsys.v2 {
}
declare namespace fm.liveswitch.xirsys.v2 {
}
declare namespace fm.liveswitch.xirsys.v2 {
}
declare namespace fm.liveswitch.xirsys.v3 {
    /**
     <div>
     A XirSys v3 client.
     </div>

    */
    class Client {
        getTypeString(): string;
        private fmliveswitchxirsysv3ClientInit;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-constructor'>&nbsp;</span>**/
        /**
         <div>
         Initializes a new instance of the `fm.liveswitch.xirsys.v3.client` class.
         </div>

        @param {string} ident The "ident" value.
        @param {string} secret The "secret" value.
        @param {string} channel The "channel" value.
        @param {boolean} secure The "secure" value.
        @return {}
        */
        constructor(ident: string, secret: string, channel: string, secure: boolean);
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-constructor'>&nbsp;</span>**/
        /**
         <div>
         Initializes a new instance of the `fm.liveswitch.xirsys.v3.client` class.
         </div>

        @param {string} ident The "ident" value.
        @param {string} secret The "secret" value.
        @param {string} channel The "channel" value.
        @return {}
        */
        constructor(ident: string, secret: string, channel: string);
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-getDefaultEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Gets the default HTTP endpoint.
         Defaults to "https://global.xirsys.net/_turn".
         </div>


        @return {string}
        */
        static getDefaultEndpoint(): string;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-setDefaultEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Sets the default HTTP endpoint.
         Defaults to "https://global.xirsys.net/_turn".
         </div>


        @param {string} value
        @return {void}
        */
        static setDefaultEndpoint(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-getChannel'>&nbsp;</span>**/
        /**
         <div>
         Gets the "channel" value.
         </div>


        @return {string}
        */
        getChannel(): string;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-getEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Gets the HTTP endpoint.
         </div>


        @return {string}
        */
        getEndpoint(): string;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-getIceServers'>&nbsp;</span>**/
        /**
         <div>
         Gets an array of XirSys ICE servers.
         </div>

        @return {fm.liveswitch.Future<fm.liveswitch.IceServer[]>}
        */
        getIceServers(): fm.liveswitch.Future<fm.liveswitch.IceServer[]>;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-getIdent'>&nbsp;</span>**/
        /**
         <div>
         Gets the "ident" value.
         </div>


        @return {string}
        */
        getIdent(): string;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-getSecret'>&nbsp;</span>**/
        /**
         <div>
         Gets the "secret" value.
         </div>


        @return {string}
        */
        getSecret(): string;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-getSecure'>&nbsp;</span>**/
        /**
         <div>
         Gets the "secure" value.
         Defaults to <c>true</c>.
         </div>


        @return {boolean}
        */
        getSecure(): boolean;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-setChannel'>&nbsp;</span>**/
        /**
         <div>
         Sets the "channel" value.
         </div>


        @param {string} value
        @return {void}
        */
        setChannel(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-setEndpoint'>&nbsp;</span>**/
        /**
         <div>
         Sets the HTTP endpoint.
         </div>


        @param {string} value
        @return {void}
        */
        setEndpoint(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-setIdent'>&nbsp;</span>**/
        /**
         <div>
         Sets the "ident" value.
         </div>


        @param {string} value
        @return {void}
        */
        setIdent(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-setSecret'>&nbsp;</span>**/
        /**
         <div>
         Sets the "secret" value.
         </div>


        @param {string} value
        @return {void}
        */
        setSecret(value: string): void;
        /**<span id='method-fm.liveswitch.xirsys.v3.Client-setSecure'>&nbsp;</span>**/
        /**
         <div>
         Sets the "secure" value.
         Defaults to <c>true</c>.
         </div>


        @param {boolean} value
        @return {void}
        */
        setSecure(value: boolean): void;
    }
}
declare namespace fm.liveswitch.xirsys.v3 {
}
declare namespace fm.liveswitch.xirsys.v3 {
}
declare namespace fm.liveswitch.xirsys.v3 {
}
declare namespace fm.liveswitch.xirsys.v3 {
}
declare namespace fm.liveswitch.xirsys {
}
declare namespace fm.liveswitch.xirsys {
}
