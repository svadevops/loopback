fm.liveswitch.Util.addOnLoad(function () {
    // Get DOM elements.
    var channelSelector = document.getElementById('channelSelector');
    var nameInput = document.getElementById('name-input');
    var gatewayUrlInput = document.getElementById('gatewayurl-input');
    var appidInput = document.getElementById('appid-input');
    var sharedsecretInput = document.getElementById('sharedsecret-input');
    var startSessionInput = document.getElementById('start-channel-input');
    var startSessionButton = document.getElementById('start-channel-button');
    var screencaptureCheckbox = document.getElementById('screencapture-checkbox');
    var receiveonlyCheckbox = document.getElementById('receiveonly-checkbox');
    var audioonlyCheckbox = document.getElementById('audioonly-checkbox');
    var joinType = document.getElementById('join-type');
    var videoResolution = document.getElementById('video-resolution');
    var appId = document.getElementById('app-id');
    var videoChat = document.getElementById('video-chat');
    var loading = document.getElementById('loading');
    var video = document.getElementById('video');
    var leaveButton = document.getElementById('leaveButton');
    var sendButton = document.getElementById('sendButton');
    var sendInput = document.getElementById('sendInput');
    var text = document.getElementById('chatDisplay');
    var toggleAudioMute = document.getElementById('toggleAudioMute');
    var toggleVideoMute = document.getElementById('toggleVideoMute');
    var toggleVideoPreview = document.getElementById('toggleVideoPreview');
    var chromeExtensionInstallButton = document.getElementById('chromeExtensionInstallButton');
    var audioDeviceList = document.getElementById('audioDeviceList');
    var videoDeviceList = document.getElementById('videoDeviceList');
    var DispUser = document.getElementById('UserDisplay');
    var full = document.getElementById('fullscreenexpand');
    videoChat.style.height = (window.innerHeight - 138 - 138).toString();
    channelSelector.style.height = (window.innerHeight - 138 - 138).toString();

    // Create new App.
    var app = new chat.App(document.getElementById('log'));
    window.App = app;
    // Create a random 6 digit number for the new channel ID.
    nameInput.value = sessionStorage.getItem('useremail');
    startSessionInput.value = (Math.floor(Math.random() * 900000) + 100000).toString();
    // Safari does not support screen-sharing.
    screencaptureCheckbox.disabled = fm.liveswitch.Util.isSafari();
    var start = function (channelId) {
        if (window.console && window.console.log) {
            window.console.log(channelId);
        }
        if (app.channelId) {
            return;
        }
        if (channelId.length < 1 || channelId.length > 255) {
            alert('Channel ID must be at least 1 and at most 255 characters long.');
            return;
        }
        var appIdFromUrl = location.href.substring(location.href.indexOf("?") + 1).replace(location.hash, "");
        if (location.href.indexOf("?") != -1 && appIdFromUrl != "") {
            app.applicationId = appIdFromUrl;
        }
        app.channelId = channelId;
        if (joinType.options[joinType.selectedIndex].text == "MCU") {
            app.mode = chat.Mode.Mcu;
        }
        else if (joinType.options[joinType.selectedIndex].text == "SFU") {
            app.mode = chat.Mode.Sfu;
        }
        else {
            app.mode = chat.Mode.Peer;
        }
        // Switch the UI context.
        location.hash = app.channelId + "&" + app.mode + "&height=" + app.videoHeight + "&width=" + app.videoWidth;

        debugger;
        app.gatewayUrl = gatewayUrlInput.value;
        // app.IceServerUrl='stun:stun.liveswitch.fm:3478';
        // app.IceServerUrl2='turn:turn.liveswitch.fm:3478';
        // app.IceServerUrl2UserName='test';
        // app.IceServerUrl2Password='pa55w0rd!';
        // app.authServerUrl1=authserverurl1.value;
        // app.authServerUrl2=authserverurl.value;
        // app.authServerUserName=authserverusername.value;
        // app.authServerPassword=authserverpassword.value;
        app.applicationId = appidInput.value;
        app.sharedsecretInput = sharedsecretInput.value;
        var audioonly = audioonlyCheckbox.checked;
        var screencapture = screencaptureCheckbox.checked;
        var receiveonly = receiveonlyCheckbox.checked;
        localStorage.setItem('screen', screencapture ? '1' : '0');
        localStorage.setItem('audioonly', audioonly ? '1' : '0');
        localStorage.setItem('receiveonly', receiveonly ? '1' : '0');
        videoChat.style.display = 'block';
        channelSelector.style.display = 'none';
        enableChatUI(false); // We disable the Chat UI now and enable it after registration.
        // Start the local media.
        if (!receiveonly) {
            fm.liveswitch.Log.info('Starting local media...');
        }
        app.startLocalMedia(video, screencapture, audioonly, receiveonly, audioDeviceList, videoDeviceList).then(function (o) {
            if (!receiveonly) {
                fm.liveswitch.Log.info('Started local media.');
            }
            // Update the UI context.
            loading.style.display = 'none';
            video.style.display = 'block';
            // Enable the media controls.
            toggleAudioMute.removeAttribute('disabled');
            toggleVideoMute.removeAttribute('disabled');
            // Register.
            fm.liveswitch.Log.info('Registering...');
            app.setUserName(nameInput.value);

            app.joinAsync(incomingMessage, peerLeft, peerJoined, clientRegistered).then(function (o) {
                fm.liveswitch.Log.info('Registered.');
                writeMessage('<b>You\'ve joined session ' + app.channelId + ' as ' + nameInput.value + '.</b>');
                // Enable the leave button.
                leaveButton.removeAttribute('disabled');
            }, function (ex) {
                fm.liveswitch.Log.error('Could not joinAsync.', ex);
                stop();
            });
        }, function (ex) {
            fm.liveswitch.Log.error('Could not start local media.', ex);
            alert('Could not start local media.\n' + (ex.message || ex.name));
            stop();
        });
    };

    var sendMessage = function () {
        var msg = sendInput.value;
        sendInput.value = '';
        if (msg != '') {
            app.sendMessage(msg);
        }
    };
    var incomingMessage = function (name, message) {
        writeMessage('<b>' + name + ':</b> ' + message);
    };
    app.incomingMessage = incomingMessage;
    if (fm.liveswitch.Util.isEdge()) {
        app.dataChannelsSupported = false;
    }
    var enableChatUI = function (enable) {
        sendButton.disabled = !enable;
        sendInput.disabled = !enable;
    };
    // After the client has registered, we should enable the sendButton and sendInput.
    var clientRegistered = function () {
        enableChatUI(true);
    };
    // After the client has unregistered, we should disable the sendButton and sendInput. 
    var clientUnregistered = function () {
        enableChatUI(false);
    };
    var peerLeft = function (name) {
        writeMessage('<b>' + name + ' left.</b>');

    };

    var peerJoined = function (name) {

        writeMessage('<b>' + name + ' joined.</b>');
    };
    var writeMessage = function (msg) {
        var content = document.createElement('p');
        content.innerHTML = msg;
        text.appendChild(content);
        text.scrollTop = text.scrollHeight;
    };
    var stop = function () {
        if (!app.channelId) {
            return;
        }
        app.channelId = '';
        // Disable the leave button.
        leaveButton.setAttribute('disabled', 'disabled');
        fm.liveswitch.Log.info('Unregistering...');
        app.leaveAsync(clientUnregistered).then(function (o) {
            fm.liveswitch.Log.info('Unregistered.');
        }, function (ex) {
            fm.liveswitch.Log.error('Could not unregister.', ex);
        });
        // Stop the local media.
        fm.liveswitch.Log.info('Stopping local media...');
        app.stopLocalMedia().then(function (o) {
            fm.liveswitch.Log.info('Stopped local media.');
        }, function (ex) {
            fm.liveswitch.Log.error('Could not stop local media.', ex);
        });
        // Disable the media controls.
        toggleAudioMute.setAttribute('disabled', 'disabled');
        toggleVideoMute.setAttribute('disabled', 'disabled');
        // Update the UI context.
        video.style.display = 'none';
        loading.style.display = 'block';
        // Switch the UI context.
        channelSelector.style.display = 'block';
        videoChat.style.display = 'none';
        location.hash = '';
    };
    // Attach DOM events.
    fm.liveswitch.Util.observe(startSessionButton, 'click', function (evt) {
        evt.preventDefault();
        start(startSessionInput.value);
    });
    fm.liveswitch.Util.observe(screencaptureCheckbox, 'click', function (evt) {
        if (this.checked) {
            if (navigator.webkitGetUserMedia && !fm.liveswitch.LocalMedia.getChromeExtensionInstalled()) {
                chromeExtensionInstallButton.setAttribute('class', 'btn btn-default');
                startSessionButton.setAttribute('disabled', 'disabled');
            }
        }
        else {
            if (navigator.webkitGetUserMedia && !fm.liveswitch.LocalMedia.getChromeExtensionInstalled()) {
                chromeExtensionInstallButton.setAttribute('class', 'btn btn-default hidden');
                startSessionButton.removeAttribute('disabled');
            }
        }
    });
    fm.liveswitch.Util.observe(startSessionInput, 'keydown', function (evt) {
        // Treat Enter as button click.
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            start(startSessionInput.value);
            return false;
        }
    });
    fm.liveswitch.Util.observe(sendInput, 'keydown', function (evt) {
        // Treat Enter as button click.
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            sendMessage();
            return false;
        }
    });
    fm.liveswitch.Util.observe(sendButton, 'click', function (evt) {
        sendMessage();
    });
    fm.liveswitch.Util.observe(leaveButton, 'click', function (evt) {
        stop();
    });
    fm.liveswitch.Util.observe(window, 'beforeunload', function (evt) {
        stop();
    });
    fm.liveswitch.Util.observe(toggleAudioMute, 'click', function (evt) {
        var muted = app.toggleAudioMute();
        toggleAudioMute.getElementsByTagName('i')[0].className = 'la mediaicon ' + (muted ? 'la-microphone-slash' : 'la-microphone');
    });
    fm.liveswitch.Util.observe(toggleVideoMute, 'click', function (evt) {
        var video = app.toggleVideoMute();
        toggleVideoMute.getElementsByTagName('i')[0].className = 'la mediaicon ' + (video ? 'la-video-camera change' : 'la-video-camera');

        if (toggleVideoMute.getElementsByTagName('i')[0].className == 'la mediaicon la-video-camera change') {
            document.getElementById('toggleVideoMute').classList.add('video_slash');
        }
        else {
            document.getElementById('toggleVideoMute').classList.remove('video_slash');
        }
    });
    fm.liveswitch.Util.observe(toggleVideoPreview, 'click', function (evt) {
        var visible = app.toggleVideoPreview();
        toggleVideoPreview.getElementsByTagName('i')[0].className = 'la mediaicon ' + (visible ? 'la-eye-slash' : 'la-eye');
    });
    fm.liveswitch.Util.observe(audioDeviceList, 'change', function (evt) {
        audioDeviceList.disabled = true;
        var option = audioDeviceList.options[audioDeviceList.selectedIndex];
        app.changeAudioDevice(option.value, option.text).then(function (o) {
            audioDeviceList.disabled = false;
        }, function (ex) {
            audioDeviceList.disabled = false;
            alert('Could not change audio device. ' + (ex.message || ex.name));
        });
    });
    fm.liveswitch.Util.observe(videoDeviceList, 'change', function (evt) {
        videoDeviceList.disabled = true;
        var option = videoDeviceList.options[videoDeviceList.selectedIndex];
        app.changeVideoDevice(option.value, option.text).then(function (o) {
            videoDeviceList.disabled = false;
        }, function (ex) {
            videoDeviceList.disabled = false;
            alert('Could not change video device. ' + (ex.message || ex.name));
        });
    });
    fm.liveswitch.Util.observe(chromeExtensionInstallButton, 'click', function () {
        if (fm.liveswitch.LocalMedia.getChromeExtensionRequiresUserGesture()) {
            // Try inline install.
            window.chrome.webstore.install(fm.liveswitch.LocalMedia.getChromeExtensionUrl(), function () {
                location.reload();
            }, function (error) {
                // Worst case scenario prompt to install manually.
                if (confirm('Inline installation failed. ' + error + '\n\nOpen Chrome Web Store?')) {
                    window.open(fm.liveswitch.LocalMedia.getChromeExtensionUrl(), '_blank');
                }
            });
        }
        else {
            // Manual installation required.
            window.open(fm.liveswitch.LocalMedia.getChromeExtensionUrl(), '_blank');
        }
    });

    // Register for handling fullscreen change event.
    fm.liveswitch.Util.observe(document, 'fullscreenchange', function (evt) { fullscreenChange(); });
    fm.liveswitch.Util.observe(document, 'webkitfullscreenchange', function (evt) { fullscreenChange(); });
    fm.liveswitch.Util.observe(document, 'mozfullscreenchange', function (evt) { fullscreenChange(); });
    fm.liveswitch.Util.observe(document, 'msfullscreenchange', function (evt) { fullscreenChange(); });
    // Register for mouse events over video element: show/hide fullscreen toggle.
    fm.liveswitch.Util.observe(video, 'mouseenter', function (evt) {
        video.classList.add('visible-controls');
    });
    fm.liveswitch.Util.observe(video, 'mouseleave', function (evt) {
        video.classList.remove('visible-controls');
    });
    // Hook click on video conference full screen toggle.
    fm.liveswitch.Util.observe(document.getElementById('fullscreen'), 'click', function (evt) {
        var fs = document.getElementById('fullscreen'), icon = document.getElementById('fullscreen-icon');
        if (icon.classList.contains('la-expand')) {
            enterFullScreen();
        }
        else {
            exitFullScreen();
        }
    });
    // Put video element into fullscreen.
    var enterFullScreen = function () {
        var icon = document.getElementById('fullscreen-icon'), video = document.getElementById('video');
        if (video.requestFullscreen) {
            video.requestFullscreen();
        }
        else if (video.mozRequestFullScreen) {
            video.mozRequestFullScreen();
        }
        else if (video.webkitRequestFullscreen) {
            video.webkitRequestFullscreen();
        }
        else if (video.msRequestFullscreen) {
            video.msRequestFullscreen();
        }
        else {
            // Add "fake" fullscreen via CSS.
            icon.classList.remove('la-expand');
            icon.classList.add('la-compress');
            video.classList.add('fs-fallback');
        }
    };

    // Take doc out of fullscreen.
    var exitFullScreen = function () {
        var icon = document.getElementById('fullscreen-icon'), video = document.getElementById('video');
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        else {
            // Remove "fake" CSS fullscreen.
            icon.classList.add('la-expand');
            icon.classList.remove('la-compress');
            video.classList.remove('fs-fallback');
        }
    };
    // Handle event: doc has entered/exited fullscreen. 
    var fullscreenChange = function () {
        var icon = document.getElementById('fullscreen-icon'), fullscreenElement = document.fullscreenElement ||
            document.mozFullScreenElement ||
            document.webkitFullscreenElement ||
            document.msFullscreenElement;
        if (fullscreenElement) {
            icon.classList.remove('la-expand');
            icon.classList.add('la-compress');
        }
        else {
            icon.classList.add('la-expand');
            icon.classList.remove('la-compress');
        }
    };
    // Automatically join if the channel ID is in the URL.
    var hash = location.href.split("#")[1];
    if (hash) {
        var args = hash.split('&');
        var channelId = args[0];
        if (args.length > 1) {
            var type = parseInt(args[1]);
            if (type) {
                joinType.selectedIndex = (type == chat.Mode.Mcu ? 0 : (type == chat.Mode.Sfu ? 1 : 2));
            }
        }
        screencaptureCheckbox.checked = localStorage.getItem('screen') === '1';
        audioonlyCheckbox.checked = localStorage.getItem('audioonly') === '1';
        receiveonlyCheckbox.checked = localStorage.getItem('receiveonly') === '1';
        if (channelId) {
            startSessionInput.value = channelId;
            start(channelId);
        }
    }
});
var isNumeric = function (evt) {
    // Only accept digit- and control-keys.
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return (charCode <= 31 || (charCode >= 48 && charCode <= 57));
};
