var chat;
(function (chat) {
    var Config = /** @class */ (function () {
        function Config() {
        }
        Config.getRegion = function () {
            return '';
        };
        Config.getGatewayUrl = function () {
          return 'https://demo.liveswitch.fm:8443/sync';
           // return 'http://192.168.1.20:8080/sync';
        };
        Config.getSharedSecret = function () {
            return '--replaceThisWithYourOwnSharedSecret--';
        };
        return Config;
    }());
    chat.Config = Config;
})(chat || (chat = {}));
