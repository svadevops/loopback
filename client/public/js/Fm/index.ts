﻿interface Window {
    chrome: any;
}

fm.liveswitch.Util.addOnLoad(() => {
    // Get DOM elements.
    let channelSelector = document.getElementById('channelSelector') as HTMLInputElement;
    let nameInput = document.getElementById('name-input') as HTMLInputElement;
    let startSessionInput = document.getElementById('start-channel-input') as HTMLInputElement;
    let startSessionButton = document.getElementById('start-channel-button') as HTMLButtonElement;
    let screencaptureCheckbox = document.getElementById('screencapture-checkbox') as HTMLInputElement;
    let receiveonlyCheckbox = document.getElementById('receiveonly-checkbox') as HTMLInputElement;
    let audioonlyCheckbox = document.getElementById('audioonly-checkbox') as HTMLInputElement;

    let joinType = document.getElementById('join-type') as HTMLSelectElement;
    let videoResolution = document.getElementById('video-resolution') as HTMLSelectElement;
    let appId = document.getElementById('app-id') as HTMLSelectElement;

    let videoChat = document.getElementById('video-chat');
    let loading = document.getElementById('loading');
    let video = document.getElementById('video');

    let leaveButton = document.getElementById('leaveButton') as HTMLButtonElement;
    let sendButton = document.getElementById('sendButton') as HTMLButtonElement;
    let sendInput = document.getElementById('sendInput') as HTMLInputElement;
    let text = document.getElementById('chatDisplay') as HTMLTextAreaElement;
    let toggleAudioMute = document.getElementById('toggleAudioMute') as HTMLButtonElement;
    let toggleVideoMute = document.getElementById('toggleVideoMute') as HTMLButtonElement;
    let toggleVideoPreview = document.getElementById('toggleVideoPreview') as HTMLButtonElement;
    let chromeExtensionInstallButton = document.getElementById('chromeExtensionInstallButton') as HTMLButtonElement;

    let audioDeviceList = document.getElementById('audioDeviceList') as HTMLSelectElement;
    let videoDeviceList = document.getElementById('videoDeviceList') as HTMLSelectElement;

    videoChat.style.height = (window.innerHeight - 138 - 138).toString();
    channelSelector.style.height = (window.innerHeight - 138 - 138).toString();

    // Create new App.
    let app = new chat.App(document.getElementById('log'));

    (window as any).App = app;

    // Create a random 6 digit number for the new channel ID.

    nameInput.value = 'Anonymous';
    startSessionInput.value = (Math.floor(Math.random() * 900000) + 100000).toString();
    
    // Safari does not support screen-sharing.
    screencaptureCheckbox.disabled = fm.liveswitch.Util.isSafari();

    let start = (channelId: string) => {
        if (window.console && window.console.log) {
            window.console.log(channelId);
        }

        if (app.channelId) {
            return;
        }

        if (channelId.length < 1 || channelId.length > 255) {
            alert('Channel ID must be at least 1 and at most 255 characters long.');
            return;
        }

        var appIdFromUrl = location.href.substring(location.href.indexOf("?") + 1).replace(location.hash, "");
        if (location.href.indexOf("?") != -1 && appIdFromUrl != "") {
            app.applicationId = appIdFromUrl;
        }

        app.channelId = channelId;
        
        if (joinType.options[joinType.selectedIndex].text == "MCU") {
            app.mode = chat.Mode.Mcu;
        } else if (joinType.options[joinType.selectedIndex].text == "SFU") {
            app.mode = chat.Mode.Sfu;
        } else {
            app.mode = chat.Mode.Peer;
        }

        // Switch the UI context.
        location.hash = app.channelId + "&" + app.mode + "&height=" + app.videoHeight + "&width=" + app.videoWidth;
        let audioonly = audioonlyCheckbox.checked;
        let screencapture = screencaptureCheckbox.checked;
        let receiveonly = receiveonlyCheckbox.checked;
        localStorage.setItem('screen', screencapture ? '1' : '0');
        localStorage.setItem('audioonly', audioonly ? '1' : '0');
        localStorage.setItem('receiveonly', receiveonly ? '1' : '0');
        videoChat.style.display = 'block';
        channelSelector.style.display = 'none';
        enableChatUI(false); // We disable the Chat UI now and enable it after registration.

        // Start the local media.
        if (!receiveonly) {
            fm.liveswitch.Log.info('Starting local media...');
        }
        app.startLocalMedia(video, screencapture, audioonly, receiveonly, audioDeviceList, videoDeviceList).then((o) => {
            if (!receiveonly) {
                fm.liveswitch.Log.info('Started local media.');
            }
            // Update the UI context.
            loading.style.display = 'none';
            video.style.display = 'block';

            // Enable the media controls.
            toggleAudioMute.removeAttribute('disabled');
            toggleVideoMute.removeAttribute('disabled');

            // Register.
            fm.liveswitch.Log.info('Registering...');
            app.setUserName(nameInput.value);
            app.joinAsync(incomingMessage, peerLeft, peerJoined, clientRegistered).then((o) => {
                fm.liveswitch.Log.info('Registered.');
                writeMessage('<b>You\'ve joined session ' + app.channelId + ' as ' + nameInput.value + '.</b>');
                // Enable the leave button.
                leaveButton.removeAttribute('disabled');
            }, (ex) => {
                fm.liveswitch.Log.error('Could not joinAsync.', ex);
                stop();
            });
        }, (ex) => {
            fm.liveswitch.Log.error('Could not start local media.', ex);
            alert('Could not start local media.\n' + (ex.message || ex.name));
            stop();
        });
    };

    let sendMessage = () => {
        var msg = sendInput.value;
        sendInput.value = '';
        if (msg != '') {
            app.sendMessage(msg);
        }
    }

    let incomingMessage = (name: string, message: string) => {
        writeMessage('<b>' + name + ':</b> ' + message);
    };
    app.incomingMessage = incomingMessage;

    if (fm.liveswitch.Util.isEdge()) {
        app.dataChannelsSupported = false;
    }

    let enableChatUI = function (enable: boolean) {
        sendButton.disabled = !enable;
        sendInput.disabled = !enable;
    }

    // After the client has registered, we should enable the sendButton and sendInput.
    let clientRegistered = function () { 
        enableChatUI(true);
    }

    // After the client has unregistered, we should disable the sendButton and sendInput. 
    let clientUnregistered = function () { 
        enableChatUI(false);
    }

    var peerLeft = function (name: string) {
        writeMessage('<b>' + name + ' left.</b>')
    };

    var peerJoined = function (name: string) {
        writeMessage('<b>' + name + ' joined.</b>');
    };

    let writeMessage = (msg: string) => {
        var content = document.createElement('p');
        content.innerHTML = msg;
        text.appendChild(content);
        text.scrollTop = text.scrollHeight;
    };

    let stop = () => {
        if (!app.channelId) {
            return;
        }

        app.channelId = '';

        // Disable the leave button.
        leaveButton.setAttribute('disabled', 'disabled');

        fm.liveswitch.Log.info('Unregistering...');
        app.leaveAsync(clientUnregistered).then((o) => {
            fm.liveswitch.Log.info('Unregistered.');
        }, (ex) => {
                fm.liveswitch.Log.error('Could not unregister.', ex);
        });

        // Stop the local media.
        fm.liveswitch.Log.info('Stopping local media...');
        app.stopLocalMedia().then((o) => {
            fm.liveswitch.Log.info('Stopped local media.');
        },
        (ex) => {
            fm.liveswitch.Log.error('Could not stop local media.', ex);
        });

        // Disable the media controls.
        toggleAudioMute.setAttribute('disabled', 'disabled');
        toggleVideoMute.setAttribute('disabled', 'disabled');

        // Update the UI context.
        video.style.display = 'none';
        loading.style.display = 'block';

        // Switch the UI context.
        channelSelector.style.display = 'block';
        videoChat.style.display = 'none';
        location.hash = '';
    };

    // Attach DOM events.
    fm.liveswitch.Util.observe(startSessionButton, 'click', (evt: any) => {
        evt.preventDefault();
        start(startSessionInput.value);
    });
    fm.liveswitch.Util.observe(screencaptureCheckbox, 'click', function (evt: any) {
        if (this.checked) {
            if ((navigator as any).webkitGetUserMedia && !fm.liveswitch.LocalMedia.getChromeExtensionInstalled()) {
                chromeExtensionInstallButton.setAttribute('class', 'btn btn-default');
                startSessionButton.setAttribute('disabled', 'disabled');
            }
        } else {
            if ((navigator as any).webkitGetUserMedia && !fm.liveswitch.LocalMedia.getChromeExtensionInstalled()) {
                chromeExtensionInstallButton.setAttribute('class', 'btn btn-default hidden');
                startSessionButton.removeAttribute('disabled');
            }
        }
    });
    fm.liveswitch.Util.observe(startSessionInput, 'keydown', (evt: any) => {
        // Treat Enter as button click.
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            start(startSessionInput.value);
            return false;
        }
    });
    fm.liveswitch.Util.observe(sendInput, 'keydown', (evt: any) => {
        // Treat Enter as button click.
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            sendMessage();
            return false;
        }
    });
    fm.liveswitch.Util.observe(sendButton, 'click', (evt: any) => {
        sendMessage();
    });
    fm.liveswitch.Util.observe(leaveButton, 'click', (evt: any) => {
        stop();
    });
    fm.liveswitch.Util.observe(window, 'beforeunload', (evt: any) => {
        stop();
    });
    fm.liveswitch.Util.observe(toggleAudioMute, 'click', (evt: any) => {
        var muted = app.toggleAudioMute();
        toggleAudioMute.getElementsByTagName('i')[0].className = 'fa fa-lg ' + (muted ? 'fa-microphone-slash' : 'fa-microphone');
    });
    fm.liveswitch.Util.observe(toggleVideoMute, 'click', (evt: any) => {
        var muted = app.toggleVideoMute();
        toggleVideoMute.style.backgroundImage = muted ? 'url(images/no-cam.png)' : 'url(images/cam.png)';
    });
    fm.liveswitch.Util.observe(toggleVideoPreview, 'click', (evt: any) => {
        var visible = app.toggleVideoPreview();
        toggleVideoPreview.getElementsByTagName('i')[0].className = 'fa fa-lg ' + (visible ? 'fa-eye-slash' : 'fa-eye');
    });
    fm.liveswitch.Util.observe(audioDeviceList, 'change', function (evt: any) {
        audioDeviceList.disabled = true;
        let option = audioDeviceList.options[audioDeviceList.selectedIndex];
        app.changeAudioDevice(option.value, option.text).then((o) => {
            audioDeviceList.disabled = false;
        }, (ex) => {
            audioDeviceList.disabled = false;
            alert('Could not change audio device. ' + (ex.message || ex.name));
        });
    });
    fm.liveswitch.Util.observe(videoDeviceList, 'change', function (evt: any) {
        videoDeviceList.disabled = true;
        let option = videoDeviceList.options[videoDeviceList.selectedIndex];
        app.changeVideoDevice(option.value, option.text).then((o) => {
            videoDeviceList.disabled = false;
        }, (ex) => {
            videoDeviceList.disabled = false;
            alert('Could not change video device. ' + (ex.message || ex.name));
        });
    });
    fm.liveswitch.Util.observe(chromeExtensionInstallButton, 'click', () => {
        if (fm.liveswitch.LocalMedia.getChromeExtensionRequiresUserGesture()) {
            // Try inline install.
            (window.chrome as any).webstore.install(fm.liveswitch.LocalMedia.getChromeExtensionUrl(), () => {
                location.reload();
            }, (error: Error) => {
                // Worst case scenario prompt to install manually.
                if (confirm('Inline installation failed. ' + error + '\n\nOpen Chrome Web Store?')) {
                    window.open(fm.liveswitch.LocalMedia.getChromeExtensionUrl(), '_blank');
                }
            });
        } else {
            // Manual installation required.
            window.open(fm.liveswitch.LocalMedia.getChromeExtensionUrl(), '_blank');
        }
    });

    // Register for handling fullscreen change event.
    fm.liveswitch.Util.observe(document, 'fullscreenchange', function (evt: any) { fullscreenChange(); });
    fm.liveswitch.Util.observe(document, 'webkitfullscreenchange', function (evt: any) { fullscreenChange(); });
    fm.liveswitch.Util.observe(document, 'mozfullscreenchange', function (evt: any) { fullscreenChange(); });
    fm.liveswitch.Util.observe(document, 'msfullscreenchange', function (evt: any) { fullscreenChange(); });

    // Register for mouse events over video element: show/hide fullscreen toggle.
    fm.liveswitch.Util.observe(video, 'mouseenter', function (evt: any) {

        video.classList.add('visible-controls');
    });
    fm.liveswitch.Util.observe(video, 'mouseleave', function (evt: any) {

        video.classList.remove('visible-controls');
    });

    // Hook click on video conference full screen toggle.
    fm.liveswitch.Util.observe(document.getElementById('fullscreen'), 'click', function (evt: any) {

        var fs = document.getElementById('fullscreen'),
            icon = document.getElementById('fullscreen-icon');

        if (icon.classList.contains('fa-expand')) {
            enterFullScreen();
        }
        else {
            exitFullScreen();
        }
    });

    // Put video element into fullscreen.
    var enterFullScreen = function () {

        var icon = document.getElementById('fullscreen-icon'),
            video = document.getElementById('video');

        if (video.requestFullscreen) {
            video.requestFullscreen();
        } else if (video.mozRequestFullScreen) {
            video.mozRequestFullScreen();
        } else if (video.webkitRequestFullscreen) {
            video.webkitRequestFullscreen();
        } else if (video.msRequestFullscreen) {
            video.msRequestFullscreen();
        }
        else {
            // Add "fake" fullscreen via CSS.
            icon.classList.remove('fa-expand');
            icon.classList.add('fa-compress');
            video.classList.add('fs-fallback');
        }
    };

    // Take doc out of fullscreen.
    var exitFullScreen = function () {

        var icon = document.getElementById('fullscreen-icon'),
            video = document.getElementById('video');

        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        else {
            // Remove "fake" CSS fullscreen.
            icon.classList.add('fa-expand');
            icon.classList.remove('fa-compress');
            video.classList.remove('fs-fallback');
        }
    };

    // Handle event: doc has entered/exited fullscreen. 
    var fullscreenChange = function () {

        var icon = document.getElementById('fullscreen-icon'),
            fullscreenElement = document.fullscreenElement ||
                document.mozFullScreenElement ||
                document.webkitFullscreenElement ||
                document.msFullscreenElement;

        if (fullscreenElement) {
            icon.classList.remove('fa-expand');
            icon.classList.add('fa-compress');
        }
        else {
            icon.classList.add('fa-expand');
            icon.classList.remove('fa-compress');
        }
    };

    // Automatically join if the channel ID is in the URL.
    var hash = location.href.split("#")[1];
    if (hash) {
        var args = hash.split('&');
        var channelId = args[0];

        if (args.length > 1) {
            var type = parseInt(args[1]) as chat.Mode;

            if (type) {
                joinType.selectedIndex = (type == chat.Mode.Mcu ? 0 : (type == chat.Mode.Sfu ? 1 : 2));
            }
        }

        screencaptureCheckbox.checked = localStorage.getItem('screen') === '1';
        audioonlyCheckbox.checked = localStorage.getItem('audioonly') === '1';
        receiveonlyCheckbox.checked = localStorage.getItem('receiveonly') === '1';
        if (channelId) {
            startSessionInput.value = channelId;
            start(channelId);
        }
    }
});

var isNumeric = (evt: any) => {
    // Only accept digit- and control-keys.
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return (charCode <= 31 || (charCode >= 48 && charCode <= 57));
};

// So TS does not complain we extend document and element typings
interface Document {
    msExitFullscreen: any;
    mozCancelFullScreen: any;
    mozFullScreenElement: any;
    msFullscreenElement: any;
}


interface HTMLElement {
    msRequestFullscreen(): void;
    mozRequestFullScreen(): void;
}