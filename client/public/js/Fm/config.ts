﻿namespace chat {
    export class Config {
        public static getRegion(): string {
            return '';
        }
        public static getGatewayUrl(): string {
            return 'https://demo.liveswitch.fm:8443/sync';
        }
        public static getSharedSecret(): string {
            return '--replaceThisWithYourOwnSharedSecret--';
        }
    }
}