import React from 'react';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import About from '../pages/About';
import Setup from '../pages/Setup/Setup';

export default [
  <Router>
    <Switch>
      <Route exact path="/about" component={About} />
      <Route exact path="/setup" component={Setup} />
    </Switch>
  </Router>
];