import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import viewer from './pages/Viewer/Viewer';
import forget_password from './pages/Login/forget_password';

export default [
    <Router>
        <Switch>
            <Route exact path="/viewer" component={viewer} />
            <Route exact path="/forget_password" component={forget_password} />
        </Switch>
    </Router>
];