import React from 'react';
import PropTypes from 'prop-types';

import shouldUpdate from 'recompose/shouldUpdate';
import ImageChat from '@material-ui/icons/Chat';
import { Link } from 'react-router-dom';
import { linkToRecord } from 'ra-core';

import Button from '@material-ui/core/Button/Button';

// useful to prevent click bubbling in a datagrid with rowClick
const HandleUserChat = (e) => console.log(e,'chat');

const ChatButton = ({
    basePath = '',
    label = 'ra.action.chat',
    record = {},
    icon = <ImageChat />,
    ...rest
}) => (
    <Button
        component={Link}
        to={`${linkToRecord(basePath, record.id)}/chat`}
        label={label}
        onClick={HandleUserChat}
        {...rest}
    >
        {icon}
    </Button>
);

ChatButton.propTypes = {
    basePath: PropTypes.string,
    label: PropTypes.string,
    record: PropTypes.object,
    icon: PropTypes.element,
};

const enhance = shouldUpdate(
    (props, nextProps) =>
        props.translate !== nextProps.translate ||
        (props.record &&
            nextProps.record &&
            props.record.id !== nextProps.record.id) ||
        props.basePath !== nextProps.basePath ||
        (props.record == null && nextProps.record != null)
);

export default enhance(ChatButton);
