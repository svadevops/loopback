import React from 'react';
import PropTypes from 'prop-types';
import shouldUpdate from 'recompose/shouldUpdate';
import ImageCall from '@material-ui/icons/Call';
import { Link } from 'react-router-dom';
import { linkToRecord } from 'ra-core';

import Button from '@material-ui/core/Button/Button';

// useful to prevent click bubbling in a datagrid with rowClick
const HandleUserCall = e => console.log(e,'call');

const CallButton = ({
    basePath = '',
    label = 'ra.action.call',
    record = {},
    icon = <ImageCall />,
    ...rest
}) => (
    <Button
        component={Link}
        to={`${linkToRecord(basePath, record.id)}/call`}
        label={label}
        onClick={HandleUserCall}
        {...rest}
    >
        {icon}
    </Button>
);

CallButton.propTypes = {
    basePath: PropTypes.string,
    label: PropTypes.string,
    record: PropTypes.object,
    icon: PropTypes.element,
};

const enhance = shouldUpdate(
    (props, nextProps) =>
        props.translate !== nextProps.translate ||
        (props.record &&
            nextProps.record &&
            props.record.id !== nextProps.record.id) ||
        props.basePath !== nextProps.basePath ||
        (props.record == null && nextProps.record != null)
);

export default enhance(CallButton);
