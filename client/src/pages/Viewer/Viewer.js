import React, { Component } from 'react';
import axios from 'axios';
import config from '../../config/config';
import './Viewer.css';
class viewer extends Component {
    constructor(props) {
        super(props);
        axios.defaults.headers.common['Authorization'] = "Basic " + sessionStorage.getItem("key");
        this.state = {
            id: "",
            activeIndex: 0,
            roomId: "",
            Fmsettings: {
                superAdmin: false,
                recordEnable: false,
                videoEnable: false,
                audioEnable: false,
                screenshareEnable: false,
                enableEcho: false,
                enableGroupChat: false,
                enablePrivate: false,
                _id: null,
                gatewayUrl: "https://35.174.54.143:8080/sync",
                sharedSecret: "--replaceThisWithYourOwnSharedSecret--",
                applicationId: "my-app-id",
                authServerUrl: "turn:turn.liveswitch.fm:3478",
                authServerUserName: null,
                authServerpassword: null,
                authServerUrl1: "stun:stun.liveswitch.fm:3478",
                channelMode: "sfu",
                channelIdFormat: "000000",
                customerId: null
            },
            Xapi: {
                object: {
                    objectType: "Activity",
                    id: "http://localhost:4000/#/viewer"
                },
                actor: {
                    objectType: "Frozen Mountain",
                    name: "vss",
                    account: {
                        name: "vss",
                        homePage: "http://www.example.com/users/"
                    }
                },
                verb: {
                    id: "http://adlnet.gov/expapi/verbs/completed"
                }
            }
        }
        this.toggleclassName = this.toggleClass.bind(this);
        this.generateid = this.generateid.bind(this);
    }
    componentDidMount() {
        this.Fmsettings();
    }
    Fmsettings = () => {
        debugger;

        axios.get(`http://${config.servHOST}:${config.servPORT}/api/getFMSettings`)
            .then((res) => {
                console.log(res);
                if (res.data) {
                    debugger;
                    console.log(res.data);
                    if (res.data) {
                        debugger;
                        sessionStorage.setItem("authServerUrl", res.data.authServer[0].authServerURI);
                        sessionStorage.setItem("authServerUserName", res.data.authServer[0].userName);
                        sessionStorage.setItem("authServerPassword", res.data.authServer[0].password);
                        sessionStorage.setItem("authServerUrl1", res.data.authServer[1].authServerURI);
                        this.setState({
                            Fmsettings: {
                                _id: res.data._id,
                                // superAdmin: res.data.superAdmin,
                                 recordEnable: res.data.recordEnable,
                                 audioEnable: res.data.audioEnable,
                                 videoEnable: res.data.videoEnable,
                                 screenshareEnable: res.data.screenshareEnable,
                                 enableEcho: res.data.enableEcho,
                                 enableGroupChat: res.data.enableGroupChat,
                                // enablePrivate: res.data.enablePrivate,
                                // gatewayUrl: res.data.gatewayUrl,
                                // sharedSecret: res.data.sharedSecret,
                                // applicationId: res.data.applicationId,
                                // // authServerUrl:res.data.authServer,
                                // authServerUrl: res.data.authServer[0].authServerURI,
                                // authServerUserName: res.data.authServer[0].userName,
                                // authServerpassword: res.data.authServer[0].password,
                                // authServerUrl1: res.data.authServer[1].authServerURI,
                                // channelMode: res.data.channelMode,
                                // channelIdFormat: res.data.channelIdFormat,
                                // customerId: res.data.customerId
                            }

                        })

                    }
                    else {
                        alert("Fmsettings is not found");
                    }

                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    handleChange = (e) => {
        //  this.setState({ roomId: e.target.value });
        if (e.target.id === "start-channel-input")
            this.setState({ roomId: e.target.value });
        if (e.target.id === "screencapture-checkbox") {
            let Fm = Object.assign({}, this.state.Fmsettings);
            Fm.screenshareEnable = e.target.checked;
            this.setState({ Fmsettings: Fm });
            console.log(this.state.Fmsettings);
        }
    }
    fullscreenchange() {
        var icon = document.getElementById('fullscreen-icon'), fullscreenElement = document.fullscreenElement ||
            document.mozFullScreenElement ||
            document.webkitFullscreenElement ||
            document.msFullscreenElement;
        if (fullscreenElement) {
            icon.classList.remove('la-expand');
            icon.classList.add('la-compress');
        }
        else {
            icon.classList.add('la-expand');

        }
    }

    openFullscreen() {
        var icon = document.getElementById('fullscreen-icon')
        var elem = document.getElementById('video');
        if (icon.classList.contains('la-expand')) {
            icon.classList.remove('la-expand');
            icon.classList.add('la-compress');
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            }
        }
        else {
            icon.classList.add('la-expand');
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
            else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            }
            else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
            else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
            else {
                // Remove "fake" CSS fullscreen.
                icon.classList.add('la-expand');
                icon.classList.remove('la-compress');
                elem.classList.remove('fs-fallback');
            }
        }


    }
    leave = () => {
        this.setState({ roomId: "" });
        axios.defaults.headers.common['Authorization'] = "Basic " + sessionStorage.getItem("key");
        axios
            .post(`http://${config.servHOST}:${config.servPORT}/api/UpdateStatement/` + this.state.id, {
                object: {
                    objectType: this.state.Xapi.object.objectType,
                    id: this.state.Xapi.object.id
                }
            })
            .then((response, err) => {
                if (err) return console.log(err);
                if (response.data) {
                    console.log(response.data);
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    handleTrigger() {
        this.refs.chatapp.click();
        axios.defaults.headers.common['Content-Type'] = "application/json";
        axios.defaults.headers.common['Authorization'] = "Basic " + sessionStorage.getItem("key");
        axios
            .post(`http://${config.servHOST}:${config.servPORT}/api/CreateStatement`, {
                object: {
                    objectType: this.state.Xapi.object.objectType,
                    id: this.state.Xapi.object.id,
                },
                actor: {
                    objectType: this.state.Xapi.actor.objectType,
                    name: this.state.Xapi.actor.name,
                    account: {
                        name: this.state.Xapi.actor.account.name,
                        homePage: this.state.Xapi.actor.account.homePage,
                    }
                },
                verb: {
                    id: this.state.Xapi.verb.id
                }
            })
            .then((response, err) => {
                if (err) return console.log(err);
                if (response.data) {
                    this.refs.chatapp.click();
                    this.setState({ id: response.data._id })
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    generateid = () => {
        axios.defaults.headers.common['Authorization'] = "Basic " + sessionStorage.getItem("key");
        axios
            .get(`http://${config.servHOST}:${config.servPORT}/api/generateRoomId`)
            .then((res) => {
                console.log(res);
                if (res.data) {
                    console.log(res.data);
                    this.setState({
                        roomId: res.data.roomId
                    })
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    toggleClass(index, e) {
        this.setState({ activeIndex: index, e });
    };
    render() {
        return (
            <div className="container-fluid p-0">
                <div className="row m-0">
                    <div className="col-xl-12 p-0">
                        <div className="row m-0 widget">
                            <div className="col-xl-2 p-0" id="sidebar">
                                <div className="sidebar-content w-100 " style={{ height: "-webkit-fill-available", width: "100%" }}>
                                    <div className="form-group col-xs-3 mt-5">
                                    </div>
                                    <div id="list-group">
                                        <ul className="friend-list list-group w-100 friends-scroll auto-scroll">
                                            <li className={this.state.activeIndex === 0 ? 'list-group-item heading active' : 'list-group-item heading'}
                                                onClick={this.toggleClass.bind(this, 0)} data-toggle="modal" data-target="#popup" >
                                                <a className="d-block" data-toggle="tab" href="#msg-0">
                                                    <div className="media">
                                                        <div className="media-body">
                                                            <h4>FM</h4>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-10 d-flex no-padding">
                                <div className="card w-100">
                                    <div className="tab-content" style={{ padding: '1rem' }}>

                                        <div className="tab-pane fade show active messages-scroll auto-scroll" style={{ flex: '1 1' }} id="msg-0">
                                            <div className="d-flex align-items-center">
                                                <div className="row">
                                                    <div className="page-header_config">
                                                        <div className="d-flex align-items-center">
                                                            <ul className="breadcrumb">
                                                                <li className="breadcrumb-item">
                                                                    <a href="">
                                                                        <i className="ti ti-home"></i>
                                                                    </a>
                                                                </li>
                                                                <li className="breadcrumb-item active ml-3">Viewer</li>
                                                                <li className="breadcrumb-item active">FM</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body d-flex flex-column no-padding">
                                                <div className="col-xl-12">
                                                    <div className="widget has-shadow">
                                                        <div className="widget-body">
                                                            <div className="row">
                                                                <div className="tab-header">
                                                                    <div className="d-flex align-items-center">
                                                                        <h4 className="page-header_config-title h4">FM</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                            <div className="tab-content">
                                                                <div id="mediachat" className="tab-pane fade">
                                                                    <h3>Media Chat</h3>
                                                                    <div id="video-chat" style={{ display: "block" }}>
                                                                        <div id="loading"></div>
                                                                        <div className="row table-row">
                                                                            <div className="col-sm-12 col-md-12 col-lg-12">
                                                                                <div className="embed-responsive embed-responsive-16by9">
                                                                                    <div id="video" className="embed-responsive-item" ><div className="expand"><a href="#" id="fullscreenexpand"><i id="fullscreen-icon" className="la la-expand" onClick={this.openFullscreen}></i></a></div></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-md-11" style={{ textAlign: "center" }}>
                                                                                <div className="">
                                                                                    <div className="btn-group" role="group">
                                                                                        <button className="btn btn-default" id="leaveButton" onClick={this.leave}>Leave</button>
                                                                                    </div>
                                                                                    <div className="btn-group" role="group">
                                                                                        <button className="btn btn-default" id="toggleAudioMute" data-toggle="tooltip" data-container="body" data-placement="bottom" title="Toggle audio mute.">
                                                                                            <i className="la la-microphone mediaicon" aria-hidden="true"></i>
                                                                                        </button>
                                                                                        <button className="btn btn-default" id="toggleVideoMute" data-toggle="tooltip" data-container="body" data-placement="bottom" title="Toggle video mute." style={{ background: "url(images/cam.png) norepeat center", backgroundsize: "20px" }}>
                                                                                            <i className="la la-video-camera mediaicon slash_line" aria-hidden="true"></i>
                                                                                        </button>
                                                                                        <button className="btn btn-default" id="toggleVideoPreview" data-toggle="tooltip" data-container="body" data-placement="bottom" title="Toggle video preview.">
                                                                                            <i className="la la-eye-slash mediaicon" aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        {/* <div className="row">
                                                                                    <div className="col-md-11">
                                                                                        Audio Device:<br />
                                                                                        <select id="audioDeviceList"></select><br />
                                                                                        Video Device:<br />
                                                                                        <select id="videoDeviceList"></select>
                                                                                    </div>
                                                                                </div> */}
                                                                    </div>
                                                                </div>
                                                                <div id="users" className="tab-pane fade">
                                                                    <h3>Users</h3>
                                                                    <div className="row"><br /></div>
                                                                    <ul className="nav nav-pills fm_menu chatwindow" id="UserDisplay" >
                                                                    </ul>
                                                                </div>
                                                                <div id="textchat" className="tab-pane fade">
                                                                    <ul class="nav nav-pills" id="UserDisplaychat">
                                                                        <li className="active" id="Home" onClick={this.test}><span className="la la-user"></span><a data-toggle="tab" href="#home">GroupChat</a></li>
                                                                    </ul>

                                                                    <div className="tab-content" id="chatInfo">
                                                                        <div id="home" className="tab-pane fade in active show">
                                                                            <div id="chatDisplay">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    {/* <ul className="nav nav-pills fm_menu chatwindow" id="UserDisplaychat">
                                                                        <li className="group_chat"><span className="la la-user"></span> Group Chat</li>
                                                                    </ul>
                                                                    <div className="row"><br /></div>
                                                                    <div className="row text_chat">
                                                                        <div className="col-md-11">
                                                                            <div className="well" id="chatDisplay"></div>
                                                                        </div>
                                                                    </div> */}
                                                                    <div className="row">
                                                                        <div className="col-md-11">
                                                                            <div className="input-group input_pos">
                                                                                <input id="sendInput" type="text" className="form-control chatwin" placeholder="Message..." />
                                                                                <span className="input-group-btn">
                                                                                    <button id="sendButton" className="btn btn-default chatsend" type="button">Send</button>
                                                                                    <br />
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row"><br /></div>
                                                                </div>
                                                            </div>
                                                            <ul className="nav nav-pills fm_menu">
                                                                <li><a data-toggle="pill" href="#mediachat" ref="chatapp">Media Chat</a></li>
                                                                <li><a data-toggle="pill" href="#users">Users</a></li>
                                                                <li><a data-toggle="pill" href="#textchat">Text Chat</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="popup" className="modal">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header meeting_popup">
                                <h4 className="modal-title title-popup">MeetingRoom</h4>
                                <button type="button" className="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                    <span className="sr-only">close</span>
                                </button>
                            </div>
                            <div className="modal-footer align-self-center border-0">
                                <input type="text" id="start-channel-input" name="start-channel-input" maxLength={255} className="form-control" placeholder="Channel ID" onChange={this.handleChange} value={this.state.roomId} />
                                <button type="button" className="btn-popup btn-primary" onClick={this.generateid}>Generate</button>
                            </div>
                            <div className="modal-footer align-self-center border-0">
                                <label>
                                    <input type="checkbox" id="screencapture-checkbox" />Capture Screen
                                    <button type="button" className="btn btn-default hidden" id="chromeExtensionInstallButton" hidden>Install Chrome Extension</button>
                                </label>
                                <div id="channelSelector" hidden>
                                    <form className="channel-form form-horizontal">
                                        <div className="form-group">
                                            <label htmlFor="name-input" className="col-sm-2 control-label" >Your Name:</label>
                                            <div className="col-sm-10">
                                                <input onClick={e => e.target.select()} className="form-control" hidden type="text" id="name-input" name="name-input" placeholder="Enter name here..." />
                                            </div>
                                            <div className="col-sm-10">
                                                <input onClick={e => e.target.select()} className="form-control" type="text" id="gatewayurl-input" name="gatewayurl-input" value={this.state.Fmsettings.gatewayUrl} placeholder="Enter gatewayurl here..." />
                                            </div>
                                            <div className="col-sm-10">
                                                <input onClick={e => e.target.select()} className="form-control" type="text" id="appid-input" name="appid-input" value={this.state.Fmsettings.applicationId} placeholder="Enter applicationId here..." />
                                            </div>
                                            <div className="col-sm-10">
                                                <input onClick={e => e.target.select()} className="form-control" type="text" id="sharedsecret-input" name="sharedsecret-input" value={this.state.Fmsettings.sharedSecret} placeholder="Enter sharedSecret here..." />
                                            </div>
                                        </div>
                                        <div className="form-group" >
                                            <label htmlFor="options" className="col-sm-2 control-label">Options:</label>
                                            <div className="col-sm-10" hidden>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="audioonly-checkbox" checked={this.state.Fmsettings.audioEnable} />Audio-only
                                                                    </label>
                                                    <label>
                                                        <input type="checkbox" id="receiveonly-checkbox" checked={this.state.Fmsettings.videoEnable} />Receive-only
                                                                    </label>
                                                    <label>

                                                        <button type="button" className="btn btn-default hidden" id="chromeExtensionInstallButton" hidden>Install Chrome Extension</button>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-inline form-group" hidden>
                                            <label htmlFor="join-type" className="col-sm-2 control-label" >Mode:</label>
                                            <div className="col-sm-10">
                                                <select id="join-type" name="join-type" value="mcu" className="form-control" >
                                                    <option value="mcu">MCU</option>
                                                    <option value="sfu">SFU</option>
                                                    <option value="peer">Peer-to-Peer</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="log" style={{ display: 'none' }} />
                            <div className="modal-footer align-self-center border-0">
                                <button type="button" id="start-channel-button" data-toggle="pill" className="btn-popup btn-primary" data-dismiss="modal" onClick={this.handleTrigger.bind(this)} data-target="#mediachat">Join</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default viewer;