import React, { Component, Fragment } from 'react';
import {
  List,
  SimpleList,
  Datagrid,
  TextField,
  EditButton,
  ShowButton
} from 'react-admin';
//import { push } from 'react-router-redux';
import { Route } from 'react-router';
import Drawer from '@material-ui/core/Drawer';
import { Config } from '../../config';
import CallButton from '../../Layout/CallButton';
import ChatButton from '../../Layout/ChatButton';
import BulkAcceptButton from '../../Layout/BulkAcceptButton';
import BulkRejectButton from '../../Layout/BulkRejectButton';
const ReviewsBulkActionButtons = props => (
  <Fragment>
      <BulkAcceptButton {...props} />
      <BulkRejectButton {...props} />
  </Fragment>
);
class UserList extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - User';
  }

  render() {
    const props = this.props;
    return (
      <Fragment>
      <List {...props} bulkActionButtons={<ReviewsBulkActionButtons />}>
        <Datagrid>
          <TextField source="id" />
          <TextField source="username" />
          <TextField type="email" source="email" />
          <EditButton />
          <ShowButton />
          <ChatButton />
          <CallButton />
        </Datagrid>
      </List>
      {/* <Route path="/users">
        {({ match }) => {
            const isMatch =
                match &&
                match.params;
            return (
                <Drawer
                    variant="persistent"
                    open={isMatch}
                    anchor="right"
                    onClose={this.handleClose}
                >
                    {/* To avoid any errors if the route does not match, we don't render at all the component in this case
                    {isMatch ? (
                        // <ReviewEdit
                        //     id={match.params.id}
                        //     onCancel={this.handleClose}
                        //     {...props}
                        // />
                        <List {...props}>
                          <SimpleList 
                             primaryText={record => record.username}
                             secondaryText={record => `${record.email} email`}
                             //tertiaryText={record => new Date(record.created_at).toLocaleDateString()}
                            // <TextField source="username" />
                            // <TextField type="email" source="email" />
                          />
                        </List>
                    ) : null}
                </Drawer>
            );
        }}
    </Route> */}
   </Fragment>
    );
  }
  handleClose = () => {
    this.props.push('/users');
  };
}

export default UserList;