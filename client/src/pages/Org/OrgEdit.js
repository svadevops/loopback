import React, { Component } from 'react';
import {
  DisabledInput,
  Edit,
  FormTab,
  TabbedForm,
  TextInput,
  ReferenceInput,
  SelectInput,
  //required,
} from 'react-admin';

import { Config } from '../../config';

class OrgEdit extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Edit Org';
  }

  render() {
    return (
      <Edit title="Edit" {...this.props}>
       <TabbedForm>
          <FormTab label="Org" path="">
            <DisabledInput source="id" />
            <TextInput source="orgname"  />
            <TextInput source="parentOrg"  />
            <TextInput source="description"  />
         </FormTab>

          <FormTab label="Org Tenant" path="tenants">
          <ReferenceInput source="org_id" reference="orgs">
          <SelectInput source="orgname" />
                </ReferenceInput>
           <TextInput source="tenantname"  />
            <TextInput source="domain"  />
            <TextInput source="tenantlogo"  />
            <TextInput source="description"  />
           
          </FormTab>
        </TabbedForm>
      </Edit>
    );
  }
}

export default OrgEdit;