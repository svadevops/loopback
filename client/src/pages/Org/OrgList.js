import React, { Component } from 'react';
import {
  List,
  SimpleList,
  Responsive,
  Datagrid,
  TextField,
  EditButton,
  ShowButton
} from 'react-admin';

import { Config } from '../../config';

class OrgList extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Organisation';
  }

  render() {
    return (
      <List {...this.props}>
      <Responsive
            small={
       <SimpleList
            primaryText={record => record.orgname}
            secondaryText={record => `${record.description} views`}
            tertiaryText={record => new Date(record.createdAt).toLocaleDateString()}
        />
            }
           
            medium={ <Datagrid>
          <TextField source="id" />
          <TextField source="orgname" />
          <TextField source="parentOrg" />
          <TextField source="description" />
          <TextField source="createdAt" />
          
          <EditButton />
          <ShowButton />
        </Datagrid>
            }
            />
      </List>
    );
  }
}

export default OrgList;