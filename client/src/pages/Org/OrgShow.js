import React, { Component } from 'react';
import {
  Show,
  Tab,
  TabbedShowLayout,
  TextField,
  DisabledInput,
  Edit,
  FormTab,
  TabbedForm,
  TextInput,
  SimpleForm,
  //ReferenceInput,
  //SelectInput,
  //required,
} from 'react-admin';

import { Config } from '../../config';

class OrgShow extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Org';
  }

  render() {
    return (
      <Show title="Org Info" {...this.props}>
        <TabbedShowLayout>
          <Tab label="Org Info">
          <TextField source="id" />
          <TextField source="orgname" />
          <TextField source="parentOrg" />
          <TextField source="description" />
          <TextField source="createdAt" />
          </Tab>
          <Tab label="Org Info">
          <Edit title="Edit" {...this.props}>
            <TabbedForm>
                <FormTab label="Org" path="">
                  <DisabledInput source="id" />
                  <TextInput source="orgname"  />
                  <TextInput source="parentOrg"  />
                  <TextInput source="description"  />
              </FormTab>

               
              </TabbedForm>
            </Edit>
            </Tab>
            <Tab label="org group">
            <Edit title="Edit" {...this.props}>
                <SimpleForm label="Org" path="Authentication">
                  <DisabledInput source="id" />
                  <TextInput source="googleClientId"  />
                  <TextInput source="googleSceret"  />
                  <TextInput source="description"  />
              </SimpleForm>
            </Edit>
          </Tab>
        </TabbedShowLayout>
      </Show>
    );
  }
}

export default OrgShow;