import OrgList from './OrgList';
import OrgCreate from './OrgCreate';
import OrgEdit from './OrgEdit';
import OrgShow from './OrgShow';

export default {
  name: 'orgs',
  list: OrgList,
  create: OrgCreate,
  edit: OrgEdit,
  show: OrgShow
  
}