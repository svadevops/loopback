import React, { Component } from 'react';
import {
  Create,
  SaveButton,
  //SimpleForm,
  TextInput,
  Toolbar,
  TabbedForm,
  FormTab,
  ReferenceInput,
  SelectInput,
  //DisabledInput,
  required,
} from 'react-admin';

import { Config } from '../../config';
const OrgCreateToolbar = ({ ...props }) => (
  <Toolbar {...props}>
    <SaveButton
      label="Save"
      redirect="show"
      submitOnEnter={true}
    />
  </Toolbar>
);

  class OrgCreate extends Component {
    componentDidMount() {
      document.title = Config.app.name + ' - Create Org';
    }
  
    render() {
      return (
        <Create {...this.props}>
          {/* <SimpleForm
            toolbar={<OrgCreateToolbar />}
          >
            <TextInput source="orgname"  />
            <TextInput source="parentOrg"  />
            <TextInput source="description"  />
          </SimpleForm> */}
           <TabbedForm toolbar={<OrgCreateToolbar />}>
           
            <FormTab label="org" path="">
            <ReferenceInput label="Group" source="groupId" reference="groups">
            <SelectInput optionText="name" />
             </ReferenceInput>
            <TextInput source="orgname" validate={required()} />
            <TextInput source="parentOrg" validate={required()} />
            <TextInput source="description"  />
            
            </FormTab>
            <FormTab label="Org Tenant" path="">
            
            <TextInput source="tenantname" validate={required()} />
            <TextInput source="domain" validate={required()} />
            <TextInput source="tenantlogo" validate={required()} />
            <TextInput source="description"  />
            
            </FormTab>
           
            
            </TabbedForm>
        </Create>
      );
    }
  }
  
  export default OrgCreate;