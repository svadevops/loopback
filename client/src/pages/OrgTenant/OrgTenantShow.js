import React, { Component } from 'react';
import {
  Show,
  Tab,
  TabbedShowLayout,
  TextField
} from 'react-admin';

import { Config } from '../../config';

class OrgShow extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Org';
  }

  render() {
    return (
      <Show title="Org Info" {...this.props}>
        <TabbedShowLayout>
          <Tab label="Org Info">
          <TextField source="id" />
          <TextField source="tenantname" />
          <TextField source="domain" />
          <TextField source="tenantlogo" />
          <TextField source="description" />
          </Tab>

         
        </TabbedShowLayout>
      </Show>
    );
  }
}

export default OrgShow;