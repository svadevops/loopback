import React, { Component } from 'react';
import {
  List,
  Datagrid,
  TextField,
  EditButton,
  ShowButton
} from 'react-admin';

import { Config } from '../../config';

class OrgList extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Organisation';
  }

  render() {
    return (
      <List {...this.props}>
        <Datagrid>
          <TextField source="id" />
          <TextField source="tenantname" />
          <TextField source="domain" />
          <TextField source="tenantlogo" />
          <TextField source="description" />
         
          <EditButton />
          <ShowButton />
        </Datagrid>
      </List>
    );
  }
}

export default OrgList;