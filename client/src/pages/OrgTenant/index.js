import OrgTenantList from './OrgTenantList';
import OrgTenantCreate from './OrgTenantCreate';
import OrgTenantEdit from './OrgTenantEdit';
import OrgTenantShow from './OrgTenantShow';

export default {
  name: 'orgTenants',
  list: OrgTenantList,
  create: OrgTenantCreate,
  edit: OrgTenantEdit,
  show: OrgTenantShow
  
}