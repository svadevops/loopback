import React, { Component } from 'react';
import {
  Create,
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  ReferenceInput,
  SelectInput,
  required,
} from 'react-admin';

import { Config } from '../../config';
const OrgTenantCreateToolbar = ({ ...props }) => (
  <Toolbar {...props}>
    <SaveButton
      label="Save"
      redirect="show"
      submitOnEnter={true}
    />
  </Toolbar>
);

  class OrgTenantCreate extends Component {
    componentDidMount() {
      document.title = Config.app.name + ' - Create OrgTenant';
    }
  
    render() {
      return (
        <Create {...this.props}>
          <SimpleForm
            toolbar={<OrgTenantCreateToolbar />}
          >
         
               
           <TextInput source="tenantname"  />
            <TextInput source="domain"  />
            <TextInput source="tenantlogo"  />
            <TextInput source="description"  />
            <ReferenceInput label="Org" source="orgId" reference="orgs">
    <SelectInput optionText="orgname" source="orgname" />
</ReferenceInput>
 
          </SimpleForm>
        </Create>
      );
    }
  }
  
  export default OrgTenantCreate;