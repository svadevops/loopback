import React, { Component } from 'react';
import {
  DisabledInput,
  Edit,
  FormTab,
  TabbedForm,
  SimpleForm,
  TextInput,
  ReferenceInput,
  SelectInput,
  required,
} from 'react-admin';

import { Config } from '../../config';

class OrgTenantEdit extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Edit OrgTenant';
  }

  render() {
    return (
      <Edit title="Edit" {...this.props}>
      <SimpleForm
           
          >
           <ReferenceInput label="Org" source="org_id" reference="orgs">
    <SelectInput optionText="orgname" />
</ReferenceInput>
           <TextInput source="tenantname"  />
            <TextInput source="domain"  />
            <TextInput source="tenantlogo"  />
            <TextInput source="description"  />
          </SimpleForm>
      </Edit>
    );
  }
}

export default OrgTenantEdit;