import React, { Component } from 'react';
import { connect } from 'react-redux';
import { translate, userLogin } from 'react-admin';
import PropTypes from 'prop-types';
import { Field, propTypes, reduxForm } from 'redux-form';
import compose from 'recompose/compose';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import {SelectInput} from 'ra-ui-materialui';
import {Config} from '../../config/index';
import ForgetPasswordButton from './forget_password';
import './LoginPageStyles.css';
const styles = theme => ({
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        height: '1px',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    card: {
        minWidth: 300,
        marginTop: '6em',
    },
    avatar: {
        margin: '1em',
        display: 'flex',
        justifyContent: 'center',
    },
    icon: {
        backgroundColor: theme.palette.secondary[500],
    },
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        marginTop: '1em',
    },
    button: {
        width: '100%',
    },
});

const renderInput = ({
    meta: { touched, error } = {},
    input: { ...inputProps },
    ...props
}) => (
    <TextField
        error={!!(touched && error)}
        helperText={touched && error}
        {...inputProps}
        {...props}
        fullWidth
    />
);
class LoginPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            site:'',
            emailId:'',
            orgs:'',
            validSite:false,
            siteStatus:false,
            site_readOnly:false,
            email_readOnly:false
        }
    }
componentDidMount = () => {
  this.ckeckSiteStatus();
}
ckeckSiteStatus = () =>{
    const request = new Request('http://18.206.122.131:3000/', {
    method: 'GET',
    headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request)
        .then(response => {
            if (response.status < 200 || response.status >= 300) {
                throw new Error(response.statusText);
            }
            this.setState({siteStatus:true,error:`Your Site is not in active`});
        })
   
}
    render() {
        const {classes, isLoading, translate}= this.props;
        const {error}=this.state;
        const handleReset = (e) => {
            console.log('reseting')
            this.setState({
                ...this.state,
                [e.target.value]:''
            })
            
        }
        const submit = (e) => {
            e.preventDefault();
            const credentials = {
                //siteUrl:this.state.siteUrl,   
                email: this.state.emailId,
                //orgs: this.state.orgs,
                password: this.state.password,
               };
               console.log(credentials)
            this.props.userLogin(credentials);
        }
        const orgChange= (e, selectedIndex) =>{
            this.setState({orgs:selectedIndex});
            //this.selectedIndex=localStorage.setItem('orgs')
        }
        
        const handleSiteValidation = () => {
            console.log('site')
            if(this.state.site){

              const site= this.state.site;
              const uid = 'wewe';
              const nav = window.navigator;
              const platform= 'web';
              const language=nav.language;
              const region = 'india';
              const deviceVersion = nav.appVersion;
              const deviceBuild = '134_bid';
                const request = new Request(Config.api() +`/sites/getSiteByName?siteUrl=${site}&uid=${uid}&language=${language}&region=${region}&platform=${platform}&deviceVersion=${deviceVersion}&deviceBuild=${deviceBuild}`, {
                method: 'GET',
                headers: new Headers({ 'Content-Type': 'application/json' }),
                })
                
                return fetch(request)
                    .then(response => {
                        if (response.status < 200 || response.status >= 300) {
                            throw new Error(response.statusText);
                        }
                        this.setState({validSite:true,site_readOnly:true,});
                    })
                }
        }
        const handleOrgs = () => {
            console.log('orgs')
            if(this.state.site && this.state.emailId){
                const {site,emailId} = this.state 
                const request = new Request(Config.api() + `/userAccounts/validateUser?site=${site}&emailId=${emailId}`, {
                method: 'GET',
                headers: new Headers({ 'Content-Type': 'application/json' }),
                })
                return fetch(request)
                    .then(response => {
                        if (response.status < 200 || response.status >= 300) {
                            throw new Error(response.statusText);
                        }
                        return response.json();
                    })
                    .then(({ orgs }) => {
                        console.log(orgs)
                        const choices =[];
                        choices.push({'id':orgs.orgIDS[0].orgId,'name':orgs.orgIDS[0].orgName}); 
                      /*  orgs.forEach((org,index) => {
                            console.log(org)
                            choices.push({'id':org.orgIDS[0].orgId,'name':org.orgIDS[0].orgName});  
                        });*/
                        this.setState({choices,email_readOnly:true});
                    });
            }

        }
        const inputChange = (e) => {
            this.setState({[e.target.name]:e.target.value});  
        }
        return (
        <div className="h-100">
            <div className="vertical-table">
                <div className="vertical-table-cell">
                    <div className="centered">
                        <div className="is-not-visible messages">
                            {this.state.siteStatus?error:null}
                        </div>
                        <div className="sign-in">
                            <div className="login_logo mb-4">
                                <img className="img-s20" src="assets/img/Logo_reg.png" alt="..."/>
                            </div>
                            <div className="input">
                                <div data-tracking-action="Submit Domain Form" data-tracking-category="Login" data-tracking-track="submit">
                                    <form onSubmit={submit.bind(this)} ref="loginForm">
                                        <div className={classes.form}>
                                            <div className={classes.input}>
                                                <Field
                                                    autoFocus
                                                    id="site"
                                                    name="site"
                                                    component={renderInput}
                                                    label={translate('ra.auth.site')}
                                                    InputProps={{
                                                        readOnly: Boolean(this.state.site_readOnly)
                                                    }}
                                                    disabled={isLoading}
                                                    onChange={inputChange.bind(this)}
                                                />
                                            </div>
                                            
                                            {this.state.validSite?
                                            <div>
                                            <div className={classes.input}>
                                                <Field
                                                    type='email'
                                                    id="emailId"
                                                    name="emailId"
                                                    component={renderInput}
                                                    label={translate('ra.auth.emailId')}
                                                    InputProps={{
                                                        readOnly: Boolean(this.state.email_readOnly)
                                                    }}
                                                    disabled={isLoading}
                                                    onChange={inputChange.bind(this)}
                                                />
                                               
                                            </div>
                
                                            {this.state.choices?
                                            <div>
                                            <div className={classes.input}>
                                                <SelectInput
                                                    className="selectInput"
                                                    name="orgs"
                                                    source="organisations" 
                                                    choices={this.state.choices} 
                                                    optionText="name"
                                                    optionValue={this.state.choices.name}
                                                    disabled={isLoading}
                                                    onChange={orgChange.bind(this)} 
                                                    style={{maxWidth:'347px',marginTop:'0px',marginBottom:'0px'}}
                                                />
                                            </div>
                                            {this.state.orgs?
                                            <div className={classes.input}>
                                                <Field
                                                    id="password"
                                                    name="password"
                                                    component={renderInput}
                                                    label={translate('ra.auth.password')}
                                                    type="password"
                                                    disabled={isLoading}
                                                    onChange={inputChange.bind(this)}
                                                />
                                            </div>:null}</div>:null}
                                            </div>:null}
                                        </div>
                                        {this.state.validSite?
                                            <div>
                                                {this.state.orgs?
                                                    <div>
                                                        <CardActions>
                                                            <Button
                                                                variant="raised"
                                                                type="submit"
                                                                color="primary"
                                                                disabled={isLoading}
                                                                className={classes.button}>
                                                                {isLoading && <CircularProgress size={25} thickness={2} />}
                                                                {translate('ra.auth.log_in')}
                                                            </Button>
                                                            <Button
                                                                variant="raised"
                                                                type="reset"
                                                                color="primary"
                                                                disabled={isLoading}
                                                                className={classes.button}
                                                                onClick={handleReset.bind(this)}
                                                            >
                                                                {isLoading && <CircularProgress size={25} thickness={2} />}
                                                                {translate('ra.auth.reset')}
                                                            </Button>
                                                        </CardActions>
                                                    </div>
                                                :<div>
                                                    <CardActions>
                                                        <Button
                                                            variant="raised"
                                                            color="primary"
                                                            disabled={isLoading}
                                                            className={classes.button}
                                                            onClick={handleOrgs.bind(this)}
                                                        >
                                                            {isLoading && <CircularProgress size={25} thickness={2} />}
                                                            {translate('ra.auth.continue')}
                                                        </Button>
                                                        <Button
                                                            variant="raised"
                                                            type="reset"
                                                            color="primary"
                                                            disabled={isLoading}
                                                            className={classes.button}
                                                            onClick={handleReset.bind(this)}
                                                        >
                                                            {isLoading && <CircularProgress size={25} thickness={2} />}
                                                            {translate('ra.auth.reset')}
                                                        </Button>
                                                    </CardActions>
                                                </div>}
                                            </div>
                                        :<Button
                                            variant="raised"
                                            color="primary"
                                            disabled={isLoading}
                                            className={classes.button}
                                            onClick={handleSiteValidation.bind(this)}>
                                            {isLoading && <CircularProgress size={25} thickness={2} />}
                                            {translate('ra.auth.continue')}
                                        </Button>}
                                    </form>
                                </div>
                            </div>
                            <div className="nav primary-themed">
                                {/* <a disable_with="One second..." data-remote="true" href="/forget-password">Forget Password?</a> */}
                                <ForgetPasswordButton />
                            </div>
                        </div>

                        <div className="powered-by">
                            <h5>Powered By</h5>
                            <img src="assets/img/Riselogo.png" alt="Company_logo" style={{width: '15%'}}/>
                            <hr className="Devider_line"/>
                        </div>
                        <ul className="Login_footer">
                            <li className="border_right"><a className="" href="/">&copy;RiseCorp Pvt.Ltd</a></li>
                            <li className="border_right"><a href="/">Contact Us</a></li>
                            <li className="border_right"><a href="/">Privacy Policy</a></li>
                            <li className="border_right"><a href="/">Terms Of Use</a></li>
                            <li className="border_right"><a href="/">Help</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
        );
    }
};
LoginPage.propTypes = {
    ...propTypes,
    classes: PropTypes.object,
    redirectTo: PropTypes.string,
};
const mapStateToProps = state => ({ isLoading: state.admin.loading > 0 });
const enhance = compose(
    withStyles(styles),
    translate,
    connect(mapStateToProps, {userLogin}),
    reduxForm({
        form: 'signIn',
        validate: (values, props) => {
            const errors = {};
            const { translate } = props;
            if (!values.site)
            errors.site = translate('ra.validation.required');
            if (!values.emailId)
                errors.emailId = translate('ra.validation.required');
            if (values.emailId && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailId)) {
                errors.emailId = translate('ra.validation.emailId');
            }
            if (!values.orgs)
                errors.orgs = translate('ra.validation.required');
            if (!values.password)
                errors.password = translate('ra.validation.required');
            return errors;
        },
    })
);

export default enhance(LoginPage);