import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { change, submit, isSubmitting } from 'redux-form';
import {
    fetchEnd,
    fetchStart,
    required,
    showNotification,
    crudGetMatching,
    Button,
    SaveButton,
    SimpleForm,
    TextInput,
    CREATE,
    REDUX_FORM_NAME
} from 'react-admin';
//import IconContentAdd from '@material-ui/icons/Add';
import IconCancel from '@material-ui/icons/Cancel';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

import {dataProvider} from 'react-admin-loopback';
import {Config} from '../../config/index';

class ForgetPasswordButton extends Component {
    state = {
        emailId:'',
        error: false,
        showDialog: false,
        successMsg:''
    };

    handleClick = () => {
        this.setState({ showDialog: true });
    };

    handleCloseClick = () => {
        this.setState({ showDialog: false });
    };

    handleSendClick = () => {
        //const { submit } = this.props;
        const emailId= this.state
        const request = new Request(Config.api() +'/userAccounts/requestPasswordReset',emailId, {
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            })
            
            return fetch(request)
                .then(response => {
                    if (response.status < 200 || response.status >= 300) {
                        alert('invlid email');
                        throw new Error(response.statusText);
                    }
                    this.setState({site_readOnly:true,successMsg:'rest password link sent to your mailId'});
                    alert('rest password link sent to your mailId');
                })
        
        // Trigger a submit of our custom quick create form
        // This is needed because our modal action buttons are oustide the form
        submit('post-quick-create');
    };

    handleSubmit = values => {
        const {
            change,
            crudGetMatching,
            fetchStart,
            fetchEnd,
            showNotification
        } = this.props;
        

        // Dispatch an action letting react-admin know a API call is ongoing
        fetchStart();

        // As we want to know when the new post has been created in order to close the modal, we use the
        // dataProvider directly
        dataProvider(CREATE, 'posts', { data: values })
            .then(({ data }) => {
                // Refresh the choices of the ReferenceInput to ensure our newly created post
                // always appear, even after selecting another post
                crudGetMatching(
                    'posts',
                    'comments@post_id',
                    { page: 1, perPage: 25 },
                    { field: 'id', order: 'DESC' },
                    {}
                );

                // Update the main react-admin form (in this case, the comments creation form)
                change(REDUX_FORM_NAME, 'post_id', data.id);
                this.setState({ showDialog: false });
            })
            .catch(error => {
                showNotification(error.message, 'error');
            })
            .finally(() => {
                // Dispatch an action letting react-admin know a API call has ended
                fetchEnd();
            });
    };

    render() {
        const { showDialog } = this.state;
        const { isSubmitting } = this.props;

        return (
            <Fragment>
                <Button onClick={this.handleClick} label="ra.action.forget_password">
                    {/* <IconContentAdd /> */}
                </Button>
                <Dialog
                    fullWidth
                    open={showDialog}
                    onClose={this.handleCloseClick}
                    aria-label="Forget Password"
                >
                    <DialogTitle>Forget Password</DialogTitle>
                    <DialogContent>
                        <SimpleForm
                            // We override the redux-form name to avoid collision with the react-admin main form
                            form="forget-password"
                            resource="forget_password"
                            // We override the redux-form onSubmit prop to handle the submission ourselves
                             onSubmit={this.handleSendClick}
                            // We want no toolbar at all as we have our modal actions
                            toolbar={null}
                        >
                            <TextInput type="email" source="email" validate={required()} />
                        </SimpleForm>
                    </DialogContent>
                    <DialogActions>
                        <SaveButton
                            label="ra.action.send"
                            saving={isSubmitting}
                            onClick={this.handleSendClick}
                        />
                        <Button
                            label="ra.action.cancel"
                            onClick={this.handleCloseClick}
                        >
                            <IconCancel />
                        </Button>
                    </DialogActions>
                </Dialog>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    isSubmitting: isSubmitting('forget-password')(state)
});

const mapDispatchToProps = {
    change,
    crudGetMatching,
    fetchEnd,
    fetchStart,
    showNotification,
    submit
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ForgetPasswordButton
);