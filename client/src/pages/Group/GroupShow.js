import React, { Component } from 'react';
import {
  Show,
  Tab,
  TabbedShowLayout,
  TextField
} from 'react-admin';

import { Config } from '../../config';

class GroupShow extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Org';
  }

  render() {
    return (
      <Show title="Group Info" {...this.props}>
        <TabbedShowLayout>
          <Tab label="Group Info">
          <TextField source="id" />
          <TextField source="groupId" />
          <TextField source="name" />
          <TextField source="description" />
          <TextField source="createdAt" />
          </Tab>

         
        </TabbedShowLayout>
      </Show>
    );
  }
}

export default GroupShow;