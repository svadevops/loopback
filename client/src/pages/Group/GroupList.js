import React, { Component } from 'react';
import {
  List,
  SimpleList,
  Responsive,
  Datagrid,
  TextField,
  EditButton,
  ShowButton
} from 'react-admin';

import { Config } from '../../config';

class GroupList extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Group';
  }

  render() {
    return (
      <List {...this.props}>
      <Responsive
            small={
       <SimpleList
            primaryText={record => record.name}
            secondaryText={record => `${record.description} views`}
            tertiaryText={record => new Date(record.createdAt).toLocaleDateString()}
        />
            }
           
            medium={ <Datagrid>
          <TextField source="id" />
          <TextField source="groupId" />
          <TextField source="name" />
          <TextField source="description" />
          <TextField source="createdAt" />
          
          <EditButton />
          <ShowButton />
        </Datagrid>
            }
            />
      </List>
    );
  }
}

export default GroupList;