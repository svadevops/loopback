import React, { Component } from 'react';
import {
  DisabledInput,
  Edit,
 
  FormTab,
  TabbedForm,
  TextInput,
  ReferenceInput,
  SelectInput,
  SimpleForm,
  required
} from 'react-admin';

import { Config } from '../../config';

class GroupEdit extends Component {
  componentDidMount() {
    document.title = Config.app.name + ' - Edit Group';
  }

  render() {
    return (
      <Edit title="Edit" {...this.props}>
        <TabbedForm>
          <FormTab label="group" path="">
            <DisabledInput source="id" />
            <TextInput source="groupId"  />
            <TextInput source="name"  />
            <TextInput source="description"  />
         </FormTab>
         
          </TabbedForm>
      </Edit>
    );
  }
}

export default GroupEdit;