import GroupList from './GroupList';
import GroupCreate from './GroupCreate';
import GroupEdit from './GroupEdit';
import GroupShow from './GroupShow';

export default {
  name: 'groups',
  list: GroupList,
  create: GroupCreate,
  edit: GroupEdit,
  show: GroupShow
  
}