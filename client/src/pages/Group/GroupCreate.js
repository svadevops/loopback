import React, { Component } from 'react';
import {
  Create,
  SaveButton,
  SimpleForm,

  Toolbar,
  TextInput,
  TabbedForm,
  FormTab,
  DisabledInput,
  required,
} from 'react-admin';

import { Config } from '../../config';
const GroupCreateToolbar = ({ ...props }) => (
  <Toolbar {...props}>
    <SaveButton
      label="Save"
      redirect="show"
      submitOnEnter={true}
    />
  </Toolbar>
);

  class GroupCreate extends Component {
    componentDidMount() {
      document.title = Config.app.name + ' - Create Group';
     
    }
  
    render() {
      return (
        <Create {...this.props}>
        <SimpleForm
          toolbar={<GroupCreateToolbar />}
          
        >
        <DisabledInput source="id" />
          <TextInput source="groupid"   />
            <TextInput source="name"   />
            <TextInput source="description"  />
        </SimpleForm>
       
      </Create>
          
        
       
      );
    }
  }
  
  export default GroupCreate;