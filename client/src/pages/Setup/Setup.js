import React, { Component } from 'react';
import { connect } from 'react-redux';
import { translate,userLogin } from 'react-admin';
import PropTypes from 'prop-types';
import { Field, propTypes, reduxForm } from 'redux-form';
import compose from 'recompose/compose';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import {SelectInput} from 'ra-ui-materialui';
//import {Config} from '../../config/index';

import './setupStyles.css';
const styles = theme => ({
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        height: '1px',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    card: {
        minWidth: 300,
        marginTop: '6em',
    },
    avatar: {
        margin: '1em',
        display: 'flex',
        justifyContent: 'center',
    },
    icon: {
        backgroundColor: theme.palette.secondary[500],
    },
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        marginTop: '1em',
    },
    button: {
        width: '100%',
    },
});

const renderInput = ({
    meta: { touched, error } = {},
    input: { ...inputProps },
    ...props
}) => (
    <TextField
        error={!!(touched && error)}
        helperText={touched && error}
        {...inputProps}
        {...props}
        fullWidth
    />
);
class LoginPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            site:'',
            emailId:'',
            orgs:'',
            siteStatus:false,
        }
    }

    render() {
        const {classes, isLoading, translate}= this.props;
        const submit = (e) => {
            e.preventDefault();
            const credentials = {
                //siteUrl:this.state.siteUrl,   
                email: this.state.emailId,
                //orgs: this.state.orgs,
                password: this.state.password,
               };
               console.log(credentials)
            this.props.userLogin(credentials);
        }
        const inputChange = (e) =>{
            this.setState({[e.target.name]:e.target.value});
        }
        return (
        <div className="h-100">
            <div className="vertical-table">
                <div className="vertical-table-cell">
                    <div className="centered">
                        <div className="is-not-visible messages">

                        </div>
                        <div className="sign-in">
                            <div className="login_logo mb-4">
                                <img className="img-s20" src="assets/img/Logo_reg.png" alt="..."/>
                            </div>
                            <div className="input">
                                <div data-tracking-action="Submit Domain Form" data-tracking-category="Login" data-tracking-track="submit">
                                    <form onSubmit={submit.bind(this)}>
                                        <div className={classes.form}>
                                            <div className={classes.input}>
                                                <Field
                                                    autoFocus
                                                    id="site"
                                                    name="site"
                                                    component={renderInput}
                                                    label={translate('ra.auth.site')}
                                                    disabled={isLoading}
                                                    onBlur={inputChange.bind(this)}
                                                />
                                            </div>
                                           
                                            <div className={classes.input}>
                                                <Field
                                                    type='email'
                                                    id="emailId"
                                                    name="emailId"
                                                    component={renderInput}
                                                    label={translate('ra.auth.emailId')}
                                                    disabled={isLoading}
                                                    onBlur={inputChange.bind(this)}
                                                />
                                            </div>
                                           
                                            <div className={classes.input}>
                                                <SelectInput
                                                    className="selectInput"
                                                    name="orgs"
                                                    source="organisations" 
                                                    choices={this.state.choices} 
                                                    optionText="name"
                                                    optionValue={this.state.choices.name}
                                                    disabled={isLoading}
                                                    onChange={inputChange.bind(this)} 
                                                    style={{maxWidth:'347px'}}
                                                />
                                            </div>
                                            <div className={classes.input}>
                                                <Field
                                                    id="password"
                                                    name="password"
                                                    component={renderInput}
                                                    label={translate('ra.auth.password')}
                                                    type="password"
                                                    disabled={isLoading}
                                                    onChange={inputChange.bind(this)}
                                                />
                                            </div>
                                        </div>
                                        <CardActions>
                                            <Button
                                                variant="raised"
                                                type="submit"
                                                color="primary"
                                                disabled={isLoading}
                                                className={classes.button}
                                            >
                                                {isLoading && <CircularProgress size={25} thickness={2} />}
                                                {translate('ra.auth.log_in')}
                                            </Button>
                                        </CardActions>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div className="powered-by">
                            <h5>Powered By</h5>
                            <img src="assets/img/Riselogo.png" alt="Company_logo" style={{width: '15%'}}/>
                            <hr className="Devider_line"/>
                        </div>
                        <ul className="Login_footer">
                            <li className="border_right"><a className="" href="/">&copy;RiseCorp Pvt.Ltd</a></li>
                            <li className="border_right"><a href="/">Contact Us</a></li>
                            <li className="border_right"><a href="/">Privacy Policy</a></li>
                            <li className="border_right"><a href="/">Terms Of Use</a></li>
                            <li className="border_right"><a href="/">Help</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
        );
    }
};
LoginPage.propTypes = {
    ...propTypes,
    classes: PropTypes.object,
    redirectTo: PropTypes.string,
};
const mapStateToProps = state => ({ isLoading: state.admin.loading > 0 });
const enhance = compose(
    withStyles(styles),
    translate,
    connect(mapStateToProps, {userLogin}),
    reduxForm({
        form: 'signIn',
        validate: (values, props) => {
            const errors = {};
            const { translate } = props;
            if (!values.site)
            errors.site = translate('ra.validation.required');
            if (!values.emailId)
                errors.emailId = translate('ra.validation.required');
            if (values.emailId && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailId)) {
                errors.emailId = translate('ra.validation.emailId');
            }
            if (!values.orgs)
                errors.orgs = translate('ra.validation.required');
            if (!values.password)
                errors.password = translate('ra.validation.required');
            return errors;
        },
    })
);

export default enhance(LoginPage);