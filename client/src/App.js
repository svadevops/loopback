import React, { Component } from 'react';
import { Admin, Resource } from 'react-admin';
import loopbackClient, { authProvider } from 'react-admin-loopback';
import englishMessages from './i18n/en';

import { Config } from './config';
import Dashboard from './pages/Dashboard';
//import Group from './pages/Group';
import Org from './pages/Org';
// import OrgTenant from './pages/OrgTenant';
import LoginPage from './pages/Login/LoginPage';
import User from './pages/User';

import './App.css';
import MyLayout from './Layout/MyLayout';
import customRoutes from './route';

const messages = {
  en: englishMessages,
};
const i18nProvider = locale => messages[locale];
class App extends Component {
  componentDidMount() {
    document.title = Config.app.name;
  }

  render() {
    return (
      
      <Admin 
        appLayout={MyLayout}
        locale="en"
        i18nProvider={i18nProvider}
        loginPage={LoginPage}
        // customRoutes={customRoutes}
        dataProvider={loopbackClient(Config.api())} 
        authProvider={authProvider(Config.api() + '/userAccounts/login')}
        dashboard={Dashboard}>
       {/* <Resource {...Group} /> */}
        <Resource {...Org} />
        {/* <Resource {...OrgTenant} /> */}
        <Resource {...User} />
      </Admin>
    );
  }
}

export default App;
