'use strict';

module.exports = function (Currencies) {
    //@ validations for uniqueness
    Currencies.validatesUniquenessOf('currencyName');


    const CurrenciesSchema = {
        "currencyName": {
            "type": "string",
            "required": true
        },
        "DeciamlPositions": {
            "type": "number",
            "required": true
        },
        "StandardSymbol": {
            "type": "string",
            "required": true
        },
        "EuroCurrency": {
            "type": "string"
        },
        "StartDate": {
            "type": "date"
        },
        "EndDate": {
            "type": "date"
        }
    };


    var Currencies = Currencies.extend('currencies', CurrenciesSchema);
};
