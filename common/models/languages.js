'use strict';

module.exports = function (Languages) {
    //@ validations for uniqueness
    Languages.validatesUniquenessOf('name');
    Languages.validatesUniquenessOf('short_id');
};
