'use strict';

module.exports = function (Domainconfigs) {
    //@ validations for uniqueness
    Domainconfigs.validatesUniquenessOf('domain_name');
    Domainconfigs.validatesUniquenessOf('SSL');
};
