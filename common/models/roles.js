'use strict';

module.exports = function (Roles) {
    Roles.disableRemoteMethodByName('create');
    Roles.disableRemoteMethodByName('prototype.updateAttributes');
};
