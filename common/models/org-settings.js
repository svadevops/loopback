'use strict';

module.exports = function (Orgsettings) {
    //@ validations for uniqueness
    Orgsettings.validatesUniquenessOf('googleStorage_email');
    Orgsettings.validatesUniquenessOf('googleStorage_clientId');
    Orgsettings.validatesUniquenessOf('googleStorage_clientSecret');

};
