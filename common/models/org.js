'use strict';
module.exports = function (Org) {
    //@ validations for uniqueness
    Org.validatesUniquenessOf('orgname');
    Org.validatesUniquenessOf('string');
};
