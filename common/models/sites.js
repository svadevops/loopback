'use strict';
const app = require('../../server/server')
const apiRequest = require('request');
module.exports = function(Sites) {
  
  app.on('started',() => {

  // @ validations

  const url = '(http[s]?:\/\/)?[^\s(["<,>]*\.[^\s[",><]*';
  Sites.validatesUniquenessOf('siteName');
  Sites.validatesUniquenessOf('siteUrl');
  Sites.validatesUniquenessOf('installUrl');
  Sites.validatesFormatOf(
    'siteUrl',
    {with: url, message: 'Must provide a valid siteUrl'});
  Sites.validatesPresenceOf('siteName', 'siteUrl', 'installUrl');
  Sites.validatesLengthOf(
    'siteName',
    {min: 3, message: {min: 'siteName is too short'}});

  const SiteSettings = Sites.app.models.SiteSettings;
  const langs = Sites.app.models.languages;
  var local = '';
  var override = Sites.find;

  // @overriding existing post order to fix issue in juggler

  Sites.find = function(filter, callback) {
    return override.apply(this, arguments);
  };

  Sites.VerifyServer=(siteUrl,cb)=>{
    siteUrl=siteUrl.toString()
    apiRequest.get(   
      {
      url : siteUrl,
      },function (error, response, body) {
          if(error)
          console.log(error)
          //cb(null,error)
          const res=JSON.parse(body)
         if( Object.keys(res).length <1)
         console.log(response,body)
        // cb(res,response)

  })
}
Sites.remoteMethod('VerifyServer',{
  http: {
    verb: 'get',
    path: '/VerifyServer' 
  },
  accepts: 
    {
      arg: 'siteUrl',
      type: 'string',
      description: 'Suite Url to get site info',
      required: true
    },
    returns: {
      arg: 'logo',
      type: 'string'
    },
   
  }
    )


    // userAccount.attachTo(app.dataSources.readData);

  Sites.getByName = function(
      siteUrl,
      uid,
      language,
      region,
      platform,
      deviceVersion,
      deviceBuild,
      cb
    ) {
    platform = platform.toLowerCase();

    langs.findOne({where: {name: language}}).then(lang => (local = lang));
    Sites.findOne({where: {siteUrl}})
        .then(doc => {
          if (Object.keys(doc).length > 0) {
            SiteSettings.findOne({where: {sites_id: doc.id}})
              .then(SiteSettingsres => {
                if (
                  Object.keys(SiteSettingsres).length > 0 &&
                  SiteSettingsres.status
                ) {
                  if (platform === 'web' && SiteSettingsres.web)
                    cb(null, {
                      device: {platform, deviceVersion},
                      browsers: SiteSettingsres.webBrowsersLists,
                      local: {region: SiteSettingsres.region, lang: local},
                      config: {
                        proto: SiteSettingsres.proto,
                        stub: SiteSettingsres.stub
                      }
                    });
                  else if ('android' === platform && SiteSettingsres.Android)
                    cb(null, {
                      device: {platform, deviceVersion},
                      device: SiteSettingsres.supportedAndroidVersion,
                      local: {region: SiteSettingsres.region, lang: local},
                      config: {
                        proto: SiteSettingsres.proto,
                        stub: SiteSettingsres.stub},
                    });
                  else if ('ios' === platform && SiteSettingsres.ios)
                    cb(null, {
                      device: {platform, deviceVersion },
                      device: SiteSettingsres.supportediosVersion,
                      local: {region: SiteSettingsres.region, lang: local },
                      config: {
                        proto: SiteSettingsres.proto,
                        stub: SiteSettingsres.stub
                      }
                    });
                  else cb(`${platform} not supported`);

                  // @need to pass default settings need to chenge afrter setp of master tables of langs and currencies
                }
              })
              .catch(err => {
                cb(err);
              });
          }
        })
        .catch(err => cb(`${siteUrl} not supported`));
  };

  // @check for site getByName

  Sites.remoteMethod('getByName', {
    http: {
      path: '/getSiteByName',
    },
    accepts: [
      {
        arg: 'siteUrl',
        type: 'string',
        description: 'Suite Url to get site info',
        required: true
      },
      {
        arg: 'uid',
        type: 'string',
        description: 'deviceId',
        required: true
      },
      {
        arg: 'language',
        type: 'string',
        description: 'for defalt language',
        required: true
      },
      {
        arg: 'region',
        type: 'string',
        description: 'for identidy time Zone',
        required: true
      },
      {
        arg: 'platform',
        type: 'string',
        description: 'for check support',
        required: true
        },
      {
        arg: 'deviceVersion',
        type: 'string',
        description: 'for check support',
        required: true
      },
      {
        arg: 'deviceBuild',
        type: 'string',
        required: true
      }
    ],
    returns: {
      arg: 'Compatibilty',
      type: 'array'
    }
  })
});
};
