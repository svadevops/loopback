'use strict';
const app = require('../../server/server');
const apiRequest = require('request');
const https= require('https');
module.exports = function (userAccount) {
    //@ validations

    userAccount.validatesUniquenessOf('phoneNumber');


    var sites = "", org = "", orgGroup = "", i = 0;

    //@ boot this funtion on app starts

    app.on('started', function () {
        sites = app.models.sites;
        org = app.models.org;
        orgGroup = app.models.orgGroup;

        //  Email = app.models.Email;
        //@ attching Email model to user Account
        //userAccount.attachTo(app.dataSources.readData);

        //@ convert email to lowercase

        userAccount.beforeRemote( 'create', async function( ctx) {
            ctx.args.data.email = ctx.args.data.email.toLowerCase();
            return ctx;
        });

        userAccount.validateUser = function (site, emailId, cb) {
            var orgIDS = [];
            sites.findOne({ where: { siteUrl: site } })
                .then(rsites => {
                    if (rsites && Object.keys(rsites).length > 0) {
                        userAccount.findOne( { email: emailId.toLowerCase() } )
                            .then(user => {
                                if (user && Object.keys(user).length > 0) {
                                    orgGroup.find({ usersId: user._id, sitesId: rsites._id })
                                        .then(orgGrp => {
                                            if (orgGrp && Object.keys(orgGrp).length > 0) {
                                                //console.log(orgGrp, 'orgGrp')
                                                orgGrp.forEach(og => {
                                                    console.log(og, 'og')
                                                    if (JSON.stringify(rsites.id) == JSON.stringify(og.sitesId)) {
                                                        org.findOne({ where: { _id: og.orgId,sitesId:rsites.id } })
                                                            .then(orgN => {
                                                                console.log(orgN.orgname,i)
                                                                var  auth= orgN.authentication 
                                                                if (Object.values(orgIDS).includes(orgN.orgname))
                                                                console.warn(orgN.orgname)
                                                                else 
                                                                    orgIDS[i++] ={orgId:orgN.id,orgName:orgN.orgname,auth}
                                                            })
                                                            .catch(err => cb(err))
                                                    }
                                                   
                                                })
                                                //cb(null,orgIDS);
                                                setTimeout(() => { i = 0; cb(null, {orgIDS}) }, 2500);
                                            }

                                            else
                                                cb(`${emailId} doesn't related to any org in ${rsites.siteUrl}`)
                                        })
                                        .catch(err => cb(err))
                                }
                                else
                                    cb(`${emailId} not exist`)
                            })
                            .catch(err => cb(err))

                    }
                    else
                        cb(`${site} not exist`)
                })
                .catch(err => cb(err))

        }
    })





    //userAccount.disableRemoteMethodByName('create');
    //userAccount.disableRemoteMethodByName('prototype.updateAttributes');

    userAccount.remoteMethod('validateUser', {
        'http': {
            'path': '/validateUser',
            'verb': 'get'
        },
        'accepts': [
            {
                arg: 'site',
                type: 'string',
                description: 'Site Url to get site info',
                required: true,

            },
            {
                arg: 'emailId',
                type: 'string',
                description: 'User Email',
                required: true,

            }
        ],
        'returns': 
            {
                arg: 'orgs',
                type: 'array',
            }
        
    });
    userAccount.requestPasswordReset = (email, cb) => {
        console.log(email)
        userAccount.findOne({ where: { email } })
            .then(userData => {
                console.log(userData)
                if (Object.keys(userData).length > 0) {
                    app.models.AccessToken.create({
                        ttl: 120960,
                        userId: userData.id
                    }).then(doc => {
                        const resetAPI = "http://18.206.122.131:3000/api/userAccounts/reset-password?access_token="
                        app.models.Email.send({
                            to: `${email} `,
                            from: 'santoshcool534@gmail.com',
                            subject: "Rest Password",
                            text: 'resetPassword',
                            html: `${resetAPI}${doc.id}`
                        }, function (err, mail) {

                            console.log(`${JSON.stringify(mail)} email sent!`);
                            cb(null, `token sent to ${email}`)
                            cb(err);
                        })
                    }

                    ).catch(err => cb(err))
                } else (cb(`inavlid Email`))
                // app.models.Email.send({
                //     to: `${email} `,
                //     from: 'santoshcool534@gmail.com',
                //     subject: 'my subject',
                //     text: 'my text',
                //     html: 'my <em>html</em>'
                // }, function (err, mail) {
                //     console.log(`${mail}email sent!`);
                //     cb(err);
                // });

                // cb(null, userData)
            })
            .catch(err => cb(err))



        //return email
    };

    userAccount.remoteMethod('requestPasswordReset', {
        'http': {
            'path': '/requestPasswordReset',
            'verb': 'post'
        },
        'accepts': {
            arg: 'email',
            type: 'string',
            description: 'Enter Email to reset password',
            required: true
        }
        ,
        'returns': {
            arg: 'token-status',
            type: 'boolean',
        }

    });

    userAccount.remoteMethod('requestPasswordReset', {
        http: {
            'path': '/requestPasswordReset',
            'verb': 'post'
        },
        accepts: {
            arg: 'email',
            type: 'string',
            description: 'Enter Email to reset password',
            required: true
        }
        ,
        returns: {
            arg: 'token-status',
            type: 'boolean',
        }

    });

    //@ ws login
    userAccount.wsLogin=(domain,email,password,cb)=>{
        apiRequest.post(   
            {
            url : "https://api.dev.workspacesarvr.net/api/login",
            form:{email,password},
            headers : { "Content-Type":"application/json","X-Customer-Domain": domain}  
            },function (error, response, body) {
                if(error)
                cb(error)
                const res=JSON.parse(body)
               if(res.status || Object.keys(res).length <1)
               cb(res)
               else{
               userAccount.findOne({where:{email}}).then(userData=>{
               userAccount.app.models.AccessToken.create({
                ttl: 120960,
                userId: userData.id,
                wstoken:res.token
               }).then(result=>{
                   cb(null,result)
               })
               .catch(err=>cb(err))
            })
               .catch(err=>cb(err))
        }
             });      

    }
    userAccount.afterRemote('wsLogin',(ctx,Output,cb)=>{
         userAccount.findOne({where:{email:ctx.args.email}})
         .then(user=>{
             user.authTokens.create({provider:'workspaces',email:ctx.args.email,clientId:ctx.args.domain,token:Output,userId:user.id
            })
             .then(res=>cb(null,res))
             .catch(err=>cb(err))
               
          }).catch(err=>cb(err)) 
    });
    
    userAccount.remoteMethod('wsLogin', {
        http: {
            'path': '/wsLogin',
            'verb': 'post'
        },
        accepts: [
            {
                arg: 'domain',
                type: 'string',
                description: 'Enter Domain',
                required: true
            
            },
            {
            arg: 'email',
            type: 'string',
            description: 'Enter Email',
            required: true
        },
        {
            arg: 'password',
            type: 'string',
            description: 'Enter Password',
            required: true
        }
    ]
        ,
        returns: {
            arg: 'token',
            type: 'object',
        }

    });
};
