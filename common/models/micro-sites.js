'use strict';
const app = require('../../server/server');
const loopback = require('loopback');

module.exports = function (Microsites) {
    //@ validations for uniqueness
    Microsites.validatesUniquenessOf('siteName');
    Microsites.validatesUniquenessOf('siteUrl');


    const User = loopback.getModel('User');

    const Languages = app.models.languages;
    const Currencies = loopback.getModel('currencies');
    Microsites.beforeRemote('create', function (ctx, data, next) {

        const { languageId, currencyId, siteId, modifiedBy, createdBy } = ctx.req.body;
        console.log(languageId, currencyId, siteId)
        if (languageId) {
            Languages.findById(languageId, function (err, doc) {
                if (err) next((err));
                if (!doc) {
                    ctx.res.statusCode = 404;
                    next(new Error(`${languageId} not found`));
                }
            });

        }
        if (currencyId) {
            Currencies.findById(currencyId, function (err, doc) {
                if (err) next((err));
                if (!doc) {
                    ctx.res.statusCode = 404;
                    next(new Error(`${currencyId} not found`));
                }
            });
        }
        if (currencyId) {
            Currencies.findById(currencyId, function (err, doc) {
                if (err) next((err));
                if (!doc) {
                    ctx.res.statusCode = 404;
                    next(new Error(`${currencyId} not found`));
                }
            });

        }
        if (modifiedBy) {
            User.findById(modifiedBy, function (err, doc) {
                if (err) next((err));
                if (!doc) {
                    ctx.res.statusCode = 404;
                    next(new Error(`access denied`));
                }
            });

        }
        if (createdBy) {
            User.findById(createdBy, function (err, doc) {
                if (err) next((err));
                if (!doc) {
                    ctx.res.statusCode = 404;
                    next(new Error(`access denied`));
                }
            });

        }

        else {
            next();
        }

    });
};
