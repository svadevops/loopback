
'use strict';
const loopback = require('loopback');
module.exports = function (licenseDeatils) {
    // var licenseDeatilsSchema = {
    //     //"org_id": "string",
    //     "licenseid": { "type": "string", "required": true },
    //     "LicenseType": { "type": "string", "required": true },
    //     "LicensePath": { "type": "string"},
    //     "createdAt": { "type": "date",  }
     
    // };
    
    // var licenseDeatils = licenseDeatils.extend('licenseDeatils', licenseDeatilsSchema);

    var app = require('../../server/server');

    licenseDeatils.beforeRemote('create', function(ctx, orgData, next) {
      var Org = app.models.org;
      var orgId = ctx.req.body.orgId;
      if (orgId) {
               var noOrg =  new Error("OrgId doesn't exist");
        Org.findById(orgId, function(err, org) {
          if (err) next(err);
          if (!org) {
            ctx.res.statusCode = 400;
            next(noOrg);
          } else {
            next();
          }
        });
      } else {
        next();
      }
    });
};