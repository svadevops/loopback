#!/bin/sh
set -e

### Configuration ###

APP_DIR=/loopback/app/
GIT_URL=https://github.com/vsiraparapu/loopback.git
RESTART_ARGS=

# Uncomment and modify the following if you installed Passenger from tarball
#export PATH=/path-to-passenger/bin:$PATH


### Automation steps ###

set -x

# Pull latest code
if [ -e $APP_DIR/code ]; then
  cd $APP_DIR/code
  git pull
 else
  git clone $GIT_URL $APP_DIR/code
  cd $APP_DIR/code
 fi

# Install dependencies
npm install --production
pm2 describe  loopback_app > /dev/null
RUNNING=$?
if [ "${RUNNING}" -ne 0 ]; then
   pm2 start /loopback/app/code/server/server.js --name=loopback_app --watch

else
  pm2 restart loopback_app

fi
