'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var bunyan = require('bunyan');
var errorHandler = require('strong-error-handler');
var rootLogger = bunyan.createLogger({
  name: 'localhost',
  serializers: {
    err: bunyan.stdSerializers.err,
    req: bunyan.stdSerializers.req
  },
  streams: [{
    level: 'info',
    path: './logs.log'
  },

  ]
});
var logger = require('loopback-component-logger')(rootLogger);

var app = module.exports = loopback();

app.start = function () {
  // start the web server
  app.use(errorHandler({
    debug: app.get('env') === 'development',
    log: true,
  }));
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.

boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
